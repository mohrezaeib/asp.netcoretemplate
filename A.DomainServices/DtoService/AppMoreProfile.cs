﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using DomainCore.DTO.AppMore;
using DomainCore.Entities.AppMore;


namespace DomainServices.DtoService.Profiles
{
    public class AppMoreProfile : Profile
    {


        public AppMoreProfile() : base()
        {
            CreateMap<AddAboutUsDto, AboutUs>(MemberList.Source)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<EditAboutUsDto, AboutUs>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<AboutUs, AboutUsDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));


            CreateMap<int, Social>()
                .EqualityComparison((src, dest) => src == dest.Id)
                .ConvertUsing((src, dst, context) => new Social { Id = src });

            CreateMap<AddSocialDto, Social>(MemberList.Source)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<EditSocialDto, Social>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<Social, SocialDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));


            CreateMap<AddFaQDto, FaQ>(MemberList.Source)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<EditFaQDto, FaQ>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<FaQ, FaQShortDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<FaQ, FaQDto>()
              .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));



            CreateMap<AddFaQCategoryDto, FaQCategory>(MemberList.Source)
           .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<EditFaQCategoryDto, FaQCategory>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<FaQCategory, FaQCategoryShortDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<FaQCategory, FaQCategoryDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<AddContactUsDto, ContactUs>(MemberList.Source)
           .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<EditContactUsStatusDto, ContactUs>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<ContactUs, ContactUsDto>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));







        }

    }


}
