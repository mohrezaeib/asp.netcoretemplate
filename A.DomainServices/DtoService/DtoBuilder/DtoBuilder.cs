﻿
using DomainCore.AppUsers;
using DomainCore.Dto.General;
using DomainCore.Dto.User;

using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Atlantis.DomainServices.Mapping
{
    public static class DtoBuilder
    {
        ////public static T CreateDto<T, TDto>(this TDto p)
        ////{




        ////    return (T) new { };
        ////}
        public static UserDto CreateUserDto(this User p)
        {
            if (p == null) return null;
            return new UserDto
            {
                BirthdayDateTime = p.Birthday,
                Birthday = p.Birthday?.Date.ToShortDateString(),
                City = DtoBuilder.CreateCityDto(p.City),
                CreatedAt = p.CreatedAt,
                Email = p.Email,
                FamilyName = p.FamilyName,
                Gender = p.Gender,
                UserId = p.Id,
                ProfileImage = DtoBuilder.CreateDocumentDto(p.ProfileImage),
                Mobile = p.Mobile,
                Name = p.Name,
                Status = p.Status,
                Bio = p.Bio,
                Roles = p.UserRole.Select(p => DtoBuilder.CreateRoleDto(p)).ToList(),
                Balance = p.Transaction != null ? p.Transaction.Where(q => q.IsSuccessful).Sum(q => q.Amount) : 0,
                Address = p.Address,
                GiftCode = p.GiftCode,
                PostalCode = p.PostalCode,
                StreetAddress = p.StreetAddress,
                FileNo = p.FileNo,
            };
        }
        public static RoleDto CreateRoleDto(this UserRole p)
        {
            if (p == null) return null;
            return new RoleDto
            {
                RoleId = p.RoleId,
                Name = p.Role.Name,

            };
        }
        public static CityDto CreateCityDto(this City p)
        {
            if (p == null) return null;
            return new CityDto
            {
                Id = p.Id,
                Name = p.Name,
                Lat = p.Lat,
                Lng = p.Lng,
                PostalCode = p.PostalCode,
                ProvinceId = p.ProvinceId,
                ProvinceName = p.Province?.Name
            };
        }
        public static ProvinceDto CreateProvinceDto(this Province p)
        {
            if (p == null) return null;
            return new ProvinceDto
            {
                Id = p.Id,
                Name = p.Name,
                ShortName = p.ShortName,
                Cities = p.Cities?.Select(DtoBuilder.CreateCityDto).ToList(),
                CountryId = p.CountryId,
            };
        }

        public static DocumentDto CreateDocumentDto(this Document p)
        {
            if (p == null) return null;
            return new DocumentDto
            {
                Id = p.Id,
                Type = p.DocumentType,
                Url = AdminSettings.Root + p.Location,
            };
        }
        public static SplashResultDto CreateSplashDto(this Update update)
        {
            var result = new SplashResultDto();
            if (update == null) result.Update = new UpdateDto { HasUpdate = false };
            else result.Update = new UpdateDto
            {
                HasUpdate = true,
                Description = update.Description,
                IsForce = update.IsForce,
                Link = update.Link,
                NewVesionCode = update.NewVersionCode,
                OS = update.OS,
                PkgName = update.PkgName
            };
            return result;
        }

        

    }
}




