﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;

using DomainCore.DTO.SupportChat;

using DomainCore.Entities.SupportChat;
using DomainCore.Enums;

using System.Linq;


namespace DomainServices.DtoService.Profiles
{
    public class SupportChatProfile : Profile 
    {
       

        public SupportChatProfile() : base()
        {
            CreateMap<Chat, ChatDto>(MemberList.Source)
                 .AfterMap((src, dest) => dest.Messages?.OrderByDescending(p=> p.CreatedAt))
              .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<Message, MessageDto>(MemberList.Source)
              .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            CreateMap<SendMessageDto, Message>(MemberList.Source)
                .AfterMap((src, dest) => dest.MessageStatus = MessageStatus.Sent)
                 .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));


            CreateMap<EditMessageByAdminDto , Message>(MemberList.Source)
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));



        }

    }
   

}
