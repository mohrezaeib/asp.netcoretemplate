﻿using AutoMapper;
using DomainCore.DTO.UserActions;
using DomainCore.Entities.UserActions;
using System.Linq;
using DomainCore.Enums;

namespace DomainServices.DtoService.Profiles
{
    public class CommentProfile : Profile 
    {
       

        public CommentProfile() : base()
        {
            //CreateMap<BodyPartCategory, BodyPart>().ConvertUsing((src, dst, context) => context.Mapper.Map<BodyPart>(src.BodyParts));
            CreateMap<AddCommentDto, Comment>(MemberList.Source).ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            CreateMap<AddCommentAnswerDto, Comment>(MemberList.Source).ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            CreateMap<EditCommentDto, Comment>().ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            CreateMap<Comment, CommenDtoForMobile>()
                .ForMember(dest => dest.Sender,
                        opt =>
                        {
                            opt.MapFrom(src => src.Sender);
                        })
                 .ForMember(dest => dest.Comments,
                        opt =>
                        {
                            opt.MapFrom(src => src.Comments.Where(p => p.Status == Status.Active).ToList());
                        }).ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            CreateMap<Comment, CommentBigDto>().ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;


        }

    }
   

}
