﻿using AutoMapper;
using DomainCore.DTO.Slider;
using DomainCore.Entities.General;


namespace DomainServices.DtoService.Profiles
{
    public class SliderProfile : Profile 
    {
       

        public SliderProfile() : base()
        {
            CreateMap<AddSliderDto, Slider>(MemberList.Source);
            CreateMap<EditSliderDto, Slider>();
            CreateMap<Slider, SliderDto>();  

        }

    }
   

}
