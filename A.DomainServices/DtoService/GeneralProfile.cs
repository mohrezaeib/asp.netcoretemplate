﻿using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.General;
using DomainCore.Dto.User;

using DomainCore.General;
using DomainServices.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace DomainServices.DtoService.Profiles
{
    public class GeneralProfile : Profile 
    {
        public GeneralProfile()
        {
           
            CreateMap<Document, DocumentDto>()
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => AdminSettings.Root+ src.Location))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.DocumentType))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            
            CreateMap<City, CityDto>().ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;
            //.ForMember(dest => dest.ProfileImageId, opt => opt.MapFrom(src => src.ProfileImageId));




        }

    }
    public class Test
    {
        public int id { get; set; }
        public string Title { get; set; }
    }
    public class TestDto
    {
        public int id { get; set; }
        public string Title { get; set; }
    }
}
