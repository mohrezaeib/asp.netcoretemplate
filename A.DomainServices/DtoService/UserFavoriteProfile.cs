﻿
using AutoMapper;

using DomainCore.DTO.UserActions;

using DomainCore.Entities.UserActions;


namespace DomainServices.DtoService.Profiles
{
    public class UserFavoriteProfile : Profile 
    {
       

        public UserFavoriteProfile() : base()
        {
            CreateMap<AddUserFavoriteDto, UserFavorite>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); 
             CreateMap<UserFavorite, UserFavoriteDto>(MemberList.Destination)
                
             .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null)); ;


        }

    }
   

}
