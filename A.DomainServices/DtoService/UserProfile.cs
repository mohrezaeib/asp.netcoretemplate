﻿using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.General;
using DomainCore.Dto.User;

using DomainCore.Entities;

using System;

using Utility.Tools.General;

namespace DomainServices.DtoService.Profiles
{
    public class UserProfile : Profile 
    {
        public UserProfile()
        {

            CreateMap<Guid, UserFile>().ConvertUsing((src, dst, context) => new UserFile { DocumentId = src, CreatedAt = Agent.Now });
            CreateMap<UserFile, DocumentDto>().ConvertUsing((src, dst, context) => new DocumentDto { Id = src.DocumentId, Type = src.Document.DocumentType , Url= src.Document.Location });
            CreateMap<DomainCore.Enums.Roles, UserRole>().ConvertUsing((src, dst, context) => new UserRole { RoleId = src, CreatedAt = Agent.Now });
            CreateMap<UserRole, RoleDto>().ConvertUsing((src, dst, context) => new RoleDto { RoleId = src.RoleId, Name = src.Role?.Name });
            CreateMap<AddUserRoleDto, UserRole>()
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<UserSetting, UserSettingDto>();
            CreateMap<UserSetting, UserSetting>();
            CreateMap<EditUserSettingDto, UserSetting>();
            CreateMap<User, UserDto>()
               // .ForMember(dest => dest.ProfileImage, opt => opt.MapFrom(src => DtoBuilder.CreateDocumentDto(src.ProfileImage)))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.UserRole))
                .ForMember(dest => dest.Documents, opt => opt.MapFrom(src => src.Documents))
                .ForMember(dest => dest.BirthdayDateTime, opt => opt.MapFrom(src => src.Birthday.HasValue ?  src.Birthday.Value : new DateTime()))
                .ForMember(dest => dest.Birthday, opt => opt.MapFrom(src => src.Birthday.HasValue ? src.Birthday.Value.Date.ToShortDateString() : null))
                .ForMember(dest => dest.HasPassword, opt => opt.MapFrom(src => !String.IsNullOrEmpty(src.Password)))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<User, UserShortDto>()
                //.ForMember(dest => dest.ProfileImage, opt => opt.MapFrom(src => DtoBuilder.CreateDocumentDto(src.ProfileImage)))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));
            ;
            CreateMap<AddUserDto, User>();
            CreateMap<AddUserByAdminDto, User>()
                .ForMember(dest => dest.CityId,
                        opt =>
                        {
                            opt.PreCondition(src => src.CityId != null);
                            opt.MapFrom(src => src.CityId);

                        })
                   .ForMember(dest => dest.UserRole,
                        opt =>
                        {
                            opt.PreCondition(src => src.Roles != null);
                            opt.MapFrom(src => src.Roles);

                        })
                   .ForMember(dest => dest.Documents,
                        opt =>
                        {
                            opt.PreCondition(src => src.Documents != null);
                            opt.MapFrom(src => src.Documents);

                        })
            ;
            CreateMap<EditProfileByAdminDto, User>()

                   .ForMember(dest => dest.CityId,
                        opt =>
                        {
                            opt.PreCondition(src => src.CityId != null);
                            opt.MapFrom(src => src.CityId);

                        })
                   .ForMember(dest => dest.UserRole,
                        opt =>
                        {
                            opt.PreCondition(src => src.Roles != null);
                            opt.MapFrom(src => src.Roles);

                        })
                   .ForMember(dest => dest.Documents,
                        opt =>
                        {
                            opt.PreCondition(src => src.Documents != null);
                            opt.MapFrom(src => src.Documents);

                        })
            
                     .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            ;
            CreateMap<EditProfileDto, User>()
                
               .ForMember(dest => dest.CityId,
                        opt =>
                        {
                            opt.PreCondition(src => src.CityId != null);
                            opt.MapFrom(src => src.CityId);

                        })
              
               .ForAllMembers(opt => opt.Condition((src, dest, sourceMember) => sourceMember != null));

            ;



        }

    }


}
