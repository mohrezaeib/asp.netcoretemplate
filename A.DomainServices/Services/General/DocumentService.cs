﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.General;
using DomainData.Interfaces;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;

using System.IO;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IUnitOfWork unit;
        private readonly IHostingEnvironment hosting;

        public DocumentService(IUnitOfWork unit, IHostingEnvironment hosting)
        {
            this.unit = unit;
            this.hosting = hosting;
        }
        public async Task<ApiResult<DocumentDto>> Send(SendDocumentDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var file = dto.File;
                string extension = Path.GetExtension(file.FileName);
                Guid guid = Guid.NewGuid();
                string fileName = guid.ToString() + extension;
                var path = hosting.WebRootPath + "/Documents";
                Directory.CreateDirectory(path);
                var HardLocation = path + $"/{fileName}";
                var HostLocation = "/Documents" + $"/{fileName}";

                if (file.Length > 0)
                {
                    using (var fileStream = new FileStream(HardLocation, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }

                Document doc = new Document()
                {
                    Id = guid,
                    Location = HostLocation,
                    CreatedAt = Agent.Now,
                    DocumentType = dto.Type,
                    Size = file.Length,
                    Name = file.FileName

                };
                await unit.Document.AddAsync(doc);
                await unit.CompleteAsync();
                result.Data = DtoBuilder.CreateDocumentDto(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }

        public async Task<ApiResult<DocumentDto>> Get(BaseByGuidDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };

            try
            {
                var doc = await unit.Document.GetAsync(dto.Id);
                result.Data = DtoBuilder.CreateDocumentDto(doc);
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }
    }
}
