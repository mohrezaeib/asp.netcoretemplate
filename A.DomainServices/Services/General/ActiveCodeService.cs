﻿

using Atlantis.DomainServices.Mapping;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.EmailServies;
using Utility.Tools.General;
using Utility.Tools.SMS.TwilioSMS;

namespace DomainServices.Services
{
    /// <summary>
    /// Active Code And Login
    /// </summary>
    public class ActiveCodeService : IActiveCodeService
    {
        private readonly ITwilioService twilioService;
        private readonly IUnitOfWork unit;
        private readonly IJwtHandler jwtHandler;
        private readonly INotification notification;
        private readonly IEmailService emailService;
        private readonly ILoginService loginService;

        public ActiveCodeService(
            ITwilioService twilioService,
            IUnitOfWork unit,
            IJwtHandler jwtHandler, 
            INotification notification, 
            IEmailService emailService, 
            ILoginService loginService
            )
        {
            this.twilioService = twilioService;
            this.unit = unit;
            this.jwtHandler = jwtHandler;
            this.notification = notification;
            this.emailService = emailService;
            this.loginService = loginService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto">SendActiveCodeDto</param>
        /// <returns></returns>
        public async Task<BaseApiResult> SendActiveCode(SendActiveCodeDto dto)
        {
            BaseApiResult result = new BaseApiResult { Message = Messages.LimitExceeded };

            try
            {
                //if (!String.IsNullOrEmpty(dto.Email))
                //{
                //    var result2 = await loginService.RegisterByEmail(new RegisterByEmailDto { Email = dto.Email });
                //    result =result2;
                //    return result;

                //}
                var now = Agent.UnixTimeNow();
                if (!unit.ActiveCode.CheckExeed(dto.Mobile))
                {
                    string Code = Agent.GenerateRandomNo(6);
                    await unit.ActiveCode.AddAsync(new ActiveCode() { Mobile = dto.Mobile, Code = Code, CreatedAt = now });
                    await unit.CompleteAsync();
                    var sent = $"{AdminSettings.SMSName}\n{AdminSettings.SMSTitle}:{Code}\n{AdminSettings.SMSCode}";
                    //TODO
                   // var messageResp2 = await twilioService.SendAsync(sent, dto.Mobile);
                    var messageResp = await notification.SendAsync(sent, dto.Mobile);
                   
                    result.Message = Messages.OK;
                    result.Status = true;
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }

        public async Task<ApiResult<UserDto>> CheckActiveCode(CheckActiveCodeDto dto)
        {
            var result = new ApiResult<UserDto> { Message = Messages.NotOK };

            try
            {

                //if (!String.IsNullOrEmpty(dto.Email))
                //{
                //    var result2 = await loginService.ConfirmEmail(new ConfirmEmailDto { Email = dto.Email , Code= dto.Code });
                //    result = result2;
                //    return result;

                //}
                var now = Agent.UnixTimeNow();
                if (unit.ActiveCode.CheckActiveCode(dto))
                {
                    result.Message = Messages.OK;
                    result.Status = true;

                    var user = await unit.Users.GetByMobileAsync(dto.Mobile);
                    
                    if (user != null)
                    {

                        if (!unit.Device.IsExist(user.Id, dto.PushId))
                        {
                            await unit.Device.AddAsync(new Device { PushId = dto.PushId, CreatedAt = now, UserId = user.Id });
                            await unit.CompleteAsync();
                        }

                    }
                    else
                    {
                        user = new User()
                        {
                            Mobile = dto.Mobile,
                            CreatedAt = now,
                            Device = new List<Device> { new Device { PushId = dto.PushId, CreatedAt = now } },
                            Status = UserStatus.Active,
                            UserRole = new List<UserRole> { new UserRole { RoleId = Roles.User, CreatedAt = now } }
                        };
                        await unit.Users.AddAsync(user);
                        await unit.CompleteAsync();
                    }
                    user = await unit.Users.GetDetailAsync(user.Id);
                    var userDto = DtoBuilder.CreateUserDto(user);
                    userDto.Token = jwtHandler.Create(user.Id);
                    result.Data = userDto;

                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }


    }
}
