﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainData.Interfaces;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class CityService : ICityService
    {
        private readonly IUnitOfWork unit;
        private readonly IHostingEnvironment hosting;

        public CityService(IUnitOfWork unit)
        {
            this.unit = unit;
        }
        public async Task<ApiListResult<ProvinceDto>> GetProvines()
        {
            var result = new ApiListResult<ProvinceDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var prs= await unit.Province.GetIncludeCtiesAsync();
                result.Data = prs.Select(DtoBuilder.CreateProvinceDto).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiListResult<CityDto>> GetByProvinceId(BaseByIntDto dto)
        {
            var result = new ApiListResult<CityDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var prs = await unit.City.GetByProvinceId(dto.Id);
                result.Data = prs.Select(DtoBuilder.CreateCityDto).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

    }
}
