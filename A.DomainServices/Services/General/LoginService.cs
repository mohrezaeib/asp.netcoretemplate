﻿

using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Interfaces;
using DomainServices.Contracts;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.EmailServies;
using Utility.Tools.General;
using Utility.Tools.SMS.TwilioSMS;

namespace DomainServices.Services
{
    public class LoginService : ILoginService
    {
        private readonly IUnitOfWork unit;
        private readonly IEncrypter encrypter;
        private readonly IJwtHandler jwtHandler;
        private readonly INotification notification;
        private readonly ITwilioService twilioService;
        private readonly IMapper mapper;
        private readonly IEmailService emailService;

        public LoginService(IUnitOfWork unit,
            IEncrypter encrypter,
            IJwtHandler jwtHandler,
            INotification notification,
            ITwilioService twilioService,
            IMapper mapper,
            IEmailService emailService
            )
        {
            this.unit = unit;
            this.encrypter = encrypter;
            this.jwtHandler = jwtHandler;
            this.notification = notification;
            this.twilioService = twilioService;
            this.mapper = mapper;
            this.emailService = emailService;
        }

        public async Task<ApiResult> ForgotPassWord(ForgotPasswordDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var user = dto.IsMobile ? await unit.Users.GetByMobileAsync(dto.UserName) : await unit.Users.GetByEmailAsync(dto.UserName);
                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                if (user.Status != UserStatus.Active) { result.Error(ErrorCodes.UserNotActivated); return result; }
             //   if (!user.UserRole.Any(p => p.RoleId == Roles.Admin)) { result.Error(ErrorCodes.UserNotAllowed); return result; }
                else
                {
                    var pass = Agent.RandomPassword(8);
                    user.SetPassword(pass, encrypter);
                    await unit.CompleteAsync();
                    //
                    var sent = $"{AdminSettings.SMSName}\n Your New Password is: {pass}";
                   // await notification.SendAsync(sent, user.Mobile);
                    await emailService.SendAsync(sent, user.Email);

                }

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        public async Task<ApiResult> ChangePassWord(ChangePasswordDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetWithDetailAsync(p => p.Id == dto.UserId);
                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                if (user.Status != UserStatus.Active) { result.Error(ErrorCodes.UserNotActivated); return result; }
                // if (!user.UserRole.Any(p => p.RoleId == Roles.Admin)) { result.Error(ErrorCodes.UserNotAllowed); return result; }
                if (!string.IsNullOrEmpty(dto.OldPassword) && !string.IsNullOrEmpty(dto.NewPassword) && user.ValidatePassword(dto.OldPassword, encrypter))
                {

                    var pass = Agent.GenerateRandomNo(10);
                    user.SetPassword(pass, encrypter);
                    await unit.CompleteAsync();
                    //
                    var sent = $"{AdminSettings.SMSName}\n Your New Password is: {pass}";
                    await notification.SendAsync(sent, user.Mobile);

                }
                else { result.Error(ErrorCodes.WrongPassword); return result; };

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }






        public async Task<ApiResult<UserDto>> Login(LoginDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var user = dto.IsMobile ? await unit.Users.GetByMobileAsync(dto.UserName) : await unit.Users.GetByEmailAsync(dto.UserName);
                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                if (user.Status != UserStatus.Active) { result.Error(ErrorCodes.UserNotActivated); return result; }
              //  if (!user.UserRole.Any(p => p.RoleId == Roles.Admin)) { result.Error(ErrorCodes.UserNotAllowed); return result; }
                if (!string.IsNullOrEmpty(dto.Password) && user.ValidatePassword(dto.Password, encrypter))
                {
                    if (!unit.Device.IsExist(user.Id, dto.PushId))
                    {
                        await unit.Device.AddAsync(new Device { PushId = dto.PushId, CreatedAt = Agent.Now, UserId = user.Id });
                        await unit.CompleteAsync();
                    }
                    user = await unit.Users.GetDetailAsync(user.Id);
                    var userDto = mapper.Map<UserDto>(user);
                    userDto.Token = jwtHandler.Create(user.Id);
                    result.Data = userDto;

                }
                else { result.Error(ErrorCodes.WrongPassword); return result; };

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<BaseApiResult> RegisterByEmail(RegisterByEmailDto dto)
        {
            var result = new ApiResult { Status = true, Message = Messages.EngOK };
            var code = Agent.GenerateRandomNo(6);
            var txt = getTemp(code);

            try
            {
                var user = await unit.Users.GetByEmailAsync(dto.Email);
               

                if (user != null)
                {
                    user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);
                    if (user.Status == UserStatus.Active && user.Password != null)
                    {
                        var d = new UserResisterDto();
                        d.User = mapper.Map<UserDto>(user);
                        d.HasPassword = true;
                        d.IsNew = false;
                        result.Data = d;
                        return result;
                    }
                    else if ((user.Status == UserStatus.Active && user.Password == null) || user.Status == UserStatus.NotConfirmed)
                    {
                        var d = new UserResisterDto();
                        
                        d.User = mapper.Map<UserDto>(user);
                        d.HasPassword = false;
                        d.IsNew = false;
                        result.Data = d;




                        user.EmailConfirmationCode = code;
                        await unit.CompleteAsync();
                        await emailService.SendAsync($"Atlatis Clinics: Your Activation Code is {code}", dto.Email);
                        return result;
                    }
                    //none of above condition
                    else if (user.Status != UserStatus.NotConfirmed)
                    {
                        result.Error(ErrorCodes.UserNotActivated);
                        return result;
                    }

                }
                else {
                    user = new User()
                    {

                        Email = dto.Email,
                        EmailConfirmationCode = code,
                        CreatedAt = Agent.Now,
                        //Device = new List<Device> { new Device { PushId = dto.PushId, CreatedAt = Agent.Now } },
                        Status = UserStatus.NotConfirmed,
                        UserRole = new List<UserRole> { new UserRole { RoleId = Roles.User, CreatedAt = Agent.Now } }
                    };
                    //if (!String.IsNullOrEmpty(dto.Password))
                    //    user.SetPassword(dto.Password, encrypter);
                    await unit.Users.AddAsync(user);
                    await unit.CompleteAsync();
                    user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);
                    unit.Complete();

                    var d = new UserResisterDto();
                    user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);

                    d.User = mapper.Map<UserDto>(user);
                    d.HasPassword = false;
                    d.IsNew = true;
                    result.Data = d;
                    //var link = AdminSettings.Root + "/api/Login/ConfirmEmail?Email=" + dto.Email + "&Code=" + code;
                    //await emailService.SendAsync(dto.Email, "New User", "Confirm your account", txt, true);
                    await emailService.SendAsync($"Atlatis Clinics: Your Activation Code is {code}",dto.Email );
                }

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<UserDto>> ConfirmEmail(ConfirmEmailDto dto)
        {
            var now = Agent.Now;
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetByEmailAsync(dto.Email);
                user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);

                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                if (dto.Code == user.EmailConfirmationCode)
                {
                    user.Status = UserStatus.Active;
                    if (!unit.Device.IsExist(user.Id, dto.PushId))
                    {
                        await unit.Device.AddAsync(new Device { PushId = dto.PushId, CreatedAt = now, UserId = user.Id });
                        await unit.CompleteAsync();
                    }

                }
                else { result.Error(ErrorCodes.WrongConfirmationCode); return result; }

                await unit.CompleteAsync();
                user = await unit.Users.GetDetailAsync(user.Id);
                var userDto = mapper.Map<UserDto>(user);
                userDto.Token = jwtHandler.Create(user.Id);
                result.Data = userDto;

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        private string getTemp(string code)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(@"
<!DOCTYPE html>
<html>
<title>Atlantis Clinics</title>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<body height: 100%; style='background-color:#000000'>

<Center>
  <h1 style='color:#ffffff'>Atlantis Clinics</h1>
    <h4  style='color:#ffffff'>Your Activition Code Is : " + code+ @"</h4>
    <img src='https://uupload.ir/files/dxef_ic_logo.png' alt='Atlantis Clinics Logo' style='width:304px;height:142px;'>

</Center>
</body>
</html>");
            var txt = doc.ParsedText;
            return txt;



        }
    }


}
