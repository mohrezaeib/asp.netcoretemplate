﻿
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;
using DomainCore.Entities.General;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class SliderCRUD : ISliderCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public SliderCRUD(IUnitOfWork unit , IMapper mapper)
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        //CRUD Admin
        #region
        //Admin area
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ApiResult<SliderDto>> EditSlider(EditSliderDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var ser = await unit.Slider.GetWithDetailAsync(p => p.Id == dto.Id);

                if (ser == null) { result.Error(ErrorCodes.EntityNotFound); return result; }
               
                ser = mapper.Map<EditSliderDto, Slider>(dto, ser);

                await unit.CompleteAsync();
                ser = await unit.Slider.GetWithDetailAsync(p => p.Id == ser.Id);
                result.Data = mapper.Map<SliderDto>(ser);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<SliderDto>> AddSlider(AddSliderDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var ser = mapper.Map<Slider>(dto);
                
                await unit.Slider.AddAsync(ser);
                await unit.CompleteAsync();
                ser = await unit.Slider.GetWithDetailAsync(p => p.Id == ser.Id);
                result.Data = mapper.Map<SliderDto>(ser);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
     
        public async Task<BaseApiResult> DeleteSlider(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var bp = await unit.Slider.GetWithDetailAsync(p => p.Id == dto.Id);
              
                unit.Slider.Remove(bp);
                try
                {
                    await unit.CompleteAsync();

                } catch(Exception e)
                {
                    result.Error(ErrorCodes.EntityCouldNotDeleted);
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiListResult<SliderDto>> GetAllSlider()
        {
            var result = new ApiListResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.Slider.GetAllWithDetailAsync();
                result.Data = resp.Select(p=> mapper.Map<SliderDto>(p)).ToList();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<SliderDto>> GetSliderById(BaseByIntDto dto)
        {
            var result = new ApiResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var ser = await unit.Slider.GetWithDetailAsync(p => p.Id == dto.Id);

                result.Data = mapper.Map<SliderDto>(ser);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiPageResult<SliderDto>> GetSliderByFilter(BaseByFilter dto)
        {
            var result = new ApiPageResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var resp = await unit.Slider.GetAllWithDetailAsync();

                var query = resp.AsQueryable().Where(p=> 
              
                (dto.From == null || dto.From >= p.CreatedAt)&&
                (dto.To == null || dto.To <= p.CreatedAt)
                );
                var paged = new Paged<Slider, SliderDto>(query, dto, mapper.Map<SliderDto>);
                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
        #endregion // 

       


    }
}
