﻿using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IDocumentService : IApplicationService
    {
         Task<ApiResult<DocumentDto>> Send(SendDocumentDto dto);
        Task<ApiResult<DocumentDto>> Get(BaseByGuidDto dto);


    }
}
