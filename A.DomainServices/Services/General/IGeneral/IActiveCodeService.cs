﻿using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface  IActiveCodeService:IApplicationService
    {
        Task<BaseApiResult> SendActiveCode(SendActiveCodeDto dto);
        Task<ApiResult<UserDto>> CheckActiveCode(CheckActiveCodeDto dto);

    }
}
