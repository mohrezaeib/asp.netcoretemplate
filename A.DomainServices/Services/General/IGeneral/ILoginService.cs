﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ILoginService : IApplicationService
    {
        Task<ApiResult<UserDto>> Login(LoginDto dto);
        
        Task<BaseApiResult> RegisterByEmail(RegisterByEmailDto dto);
        Task<ApiResult> ForgotPassWord(ForgotPasswordDto dto);
        Task<ApiResult> ChangePassWord(ChangePasswordDto dto);
        Task<ApiResult<UserDto>> ConfirmEmail(ConfirmEmailDto dto);
    }

}
