﻿using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;

using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ISliderCRUD : IApplicationService
    {
        Task<ApiResult<SliderDto>> EditSlider(EditSliderDto dto);
        Task<ApiResult<SliderDto>> AddSlider(AddSliderDto dto);
        Task<BaseApiResult> DeleteSlider(BaseByIntDto dto);
        Task<ApiListResult<SliderDto>> GetAllSlider();
        Task<ApiResult<SliderDto>> GetSliderById(BaseByIntDto dto);
    }

}
