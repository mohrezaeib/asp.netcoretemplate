﻿using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IUserService : IApplicationService
    {

        Task<ApiResult<UserDto>> EditProfile(EditProfileDto dto);
        Task<ApiResult<UserDto>> EditUserSetting(EditUserSettingDto dto);
        Task<ApiResult<UserDto>> GetUser(BaseByGuidDto dto);
        Task<ApiResult<UserDto>> SetPassword(SetPasswordByUserDto dto);
        Guid GetUserIdFromToken(string Token);
        Task<ApiResult<UserDto>> AddUserByAdmin(AddUserByAdminDto dto);
        Task<ApiResult<UserDto>> EditUserByAdmin(EditProfileByAdminDto dto);
        Task<ApiPageResult<UserDto>> GetUsersByFilter(GetUsersByFilterDto dto);



    }
}
