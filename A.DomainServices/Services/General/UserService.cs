﻿
using AutoMapper;
using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Entities;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;

using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.Auth;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unit;
        private readonly IEncrypter encrypter;
        private readonly ICurrentUser sessionContext;
        private readonly IMapper mapper;
        private readonly IRepository<UserFile> userFileRepo;
        private readonly IRepository<UserRole> userRoleRepo;

        public UserService(IUnitOfWork unit,
            IEncrypter encrypter, 
            ICurrentUser sessionContext,
            IMapper mapper,
            IRepository<UserFile> userFileRepo,
            IRepository<UserRole> userRoleRepo
            )
        {
            this.unit = unit;
            this.encrypter = encrypter;
            this.sessionContext = sessionContext;
            this.mapper = mapper;
            this.userFileRepo = userFileRepo;
            this.userRoleRepo = userRoleRepo;
        }

        public async Task<ApiResult<UserDto>> AddUserByAdmin(AddUserByAdminDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var now = Agent.Now;
                var searchResult = await unit.Users.FindAsync(p => p.Mobile == dto.Mobile || p.Email == dto.Email);
                var user = new User();

                if (searchResult.Count == 0)

                {
                    user = mapper.Map<User>(dto);
                    await unit.Users.AddAsync(user);
                    await unit.CompleteAsync();
                    //Extra 
                    user.Status = UserStatus.Active;
                    user.FileNo = dto.FileNo;
                    if (!string.IsNullOrEmpty(dto.Password))
                        user.SetPassword(dto.Password, encrypter);
                    await unit.CompleteAsync();
                    //End Extra
                    user = await unit.Users.GetDetailAsync(user.Id);
                    result.Data = mapper.Map<UserDto>(user);


                }
                else
                {
                    throw new Exception("There is already an user whith this email or mobile.");
                }

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> EditUserSetting(EditUserSettingDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                
                var user = sessionContext.UserId.HasValue? await unit.Users.GetDetailAsync(sessionContext.UserId.Value): null;
                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                var setting = mapper.Map<UserSetting>(dto);

                if (user.UserSetting == null)
                {
                    setting.UserId = user.Id;
                    unit.UserSetting.Add(setting);
                    await unit.CompleteAsync();
                    user.UserSettingId = setting.Id;
                }
                else
                {
                   
                    user.UserSetting.SendEmail = dto.SendEmail;
                    user.UserSetting.SendNotification = dto.SendNotification;
                    user.UserSetting.SendSMS = dto.SendSMS;
                }
               
                await unit.CompleteAsync();
                user = await unit.Users.GetDetailAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> EditProfile(EditProfileDto dto)
        {
           
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(ErrorCodes.UserNotFound); return result; }
                user = mapper.Map<EditProfileDto, User>(dto, user);

                await unit.CompleteAsync();
                user = await unit.Users.GetDetailAsync(user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserDto>> GetUser(BaseByGuidDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };

            try
            {
                var user = await unit.Users.GetDetailAsync(dto.Id);
                result.Data = mapper.Map<UserDto>(user);

            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;
        }
        public async Task<ApiPageResult<UserDto>> GetUsersByFilter(GetUsersByFilterDto dto)
        {
            var result = new ApiPageResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var query = unit.Users.GetByFilter(dto);
                var paged = new Paged<User, UserDto>(query, dto, mapper.Map<UserDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
            }
            catch (Exception e)
            {
                result.Error(e);
            }
            return result;

        }

        public Guid GetUserIdFromToken(string basedToken)
        {
            var userId = new Guid();
            try
            {
                if (basedToken != null && basedToken != "")
                {
                    var token = basedToken;
                    var handler = new JwtSecurityTokenHandler();
                    var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                    var IdString = jwtToken.Claims.FirstOrDefault(p => p.Type == "sub").Value;
                    userId = Guid.Parse(IdString);
                }
            }
            catch (Exception e)
            {

            }

            return userId;

        }

        private User EditUser(User user, AddUserDto dto)
        {
            user.Name = dto.Name ?? dto.Name;
            user.FamilyName = dto.FamilyName ?? dto.FamilyName;
            user.Birthday = dto.Birthday;
            user.Mobile = dto.Mobile ?? dto.Mobile;
            user.Bio = dto.Bio ?? dto.Bio;
            user.Email = dto.Email ?? dto.Email.ToLower();
            user.CityId = dto.CityId ?? dto.CityId;
            user.Gender = dto.Gender ?? dto.Gender;
            user.ProfileImageId = dto.ProfileImageId ?? dto.ProfileImageId;
            user.StreetAddress = dto.StreetAddress;
            user.PostalCode = dto.PostalCode;
            user.Address = dto.Address;

            return user;
        }

        public async Task<ApiResult<UserDto>> EditUserByAdmin(EditProfileByAdminDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {



                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(ErrorCodes.EntityNotFound); return result; }
                if (dto.Documents != null)
                {
                    userFileRepo.RemoveRange(user.Documents.Where(p => !dto.Documents.Any(x => x == p.DocumentId)));
                } 
                if (dto.Roles != null)
                {
                    userRoleRepo.RemoveRange(user.UserRole.Where(p => !dto.Roles.Any(x => x.RoleId == p.RoleId)));
                }

                user = mapper.Map<EditProfileByAdminDto, User>(dto, user);
                await unit.CompleteAsync();
                user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async  Task<ApiResult<UserDto>> SetPassword(SetPasswordByUserDto dto)
        {
            var result = new ApiResult<UserDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var user = await unit.Users.GetDetailAsync(dto.UserId);
                if (user == null) { result.Error(ErrorCodes.EntityNotFound); return result; }
               if(user.Status == UserStatus.Active)
                {
                    if (!String.IsNullOrEmpty(dto.Password))
                        user.SetPassword(dto.Password, encrypter);
                }
               else
                {
                    result.Error(ErrorCodes.UserNotActivated);
                    return result;
                }

                await unit.CompleteAsync();
                user = await unit.Users.GetWithDetailAsync(p => p.Id == user.Id);
                result.Data = mapper.Map<UserDto>(user);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
    }
}
