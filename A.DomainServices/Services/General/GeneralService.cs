﻿
using Atlantis.DomainServices.Mapping;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;
using DomainData.Interfaces;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class GeneralService : IGeneralService
    {
        private readonly IUnitOfWork unit;
 

        public GeneralService(IUnitOfWork unit)
        {
            this.unit = unit;

        }
        public async Task<ApiResult<SplashResultDto>> GetSplash(GetSplashDto dto)
        {
            var result = new ApiResult<SplashResultDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var update = await unit.Update.GetUpdateAsync(dto);
                result.Data = DtoBuilder.CreateSplashDto(update);

            }catch(Exception e)
            {
                result.Error(e);
            }


            return result;

        }
         public async Task<ApiListResult<SliderDto>> GetSliders()
        {
            var result = new ApiListResult<SliderDto> { Status = true, Message = Messages.EngOK };
            try
            {
                

            }catch(Exception e)
            {
                result.Error(e);
            }


            return result;

        }

        public async Task<ApiResult<DocumentDto>> Get(BaseByGuidDto dto)
        {
            var result = new ApiResult<DocumentDto> { Status = true, Message = Messages.EngOK };

           
            return result;

        }
    }
}
