﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainCore.Enums;
using DomainCore.General;
using DomainData.Interfaces;
using DomainServices.Contracts;
using DomainServices.DtoService.Profiles;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;
using static DomainServices.DtoService.Profiles.GeneralProfile;

namespace DomainServices.Services
{
    public class ContactUsCRUD : IContactUsCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public ContactUsCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<ContactUsDto>> EditContactUsStatus(EditContactUsStatusDto dto)
        {
            var result = new ApiResult<ContactUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var ContactUs = await unit.ContactUs.GetWithDetailAsync(p => p.Id == dto.Id);
                ContactUs = mapper.Map<EditContactUsStatusDto, ContactUs>(dto, ContactUs);
                await unit.CompleteAsync();
                ContactUs = await unit.ContactUs.GetWithDetailAsync(p => p.Id == ContactUs.Id);
                result.Data = mapper.Map< ContactUsDto>( ContactUs);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<ContactUsDto>> AddContactUs(AddContactUsDto dto)
        {
            var result = new ApiResult<ContactUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var ContactUs = mapper.Map< ContactUs>(dto);
                ContactUs.Status = ContactUsStatus.Pending;
                await unit.ContactUs.AddAsync(ContactUs);
                await unit.CompleteAsync();
                ContactUs = await unit.ContactUs.GetWithDetailAsync(p => p.Id == ContactUs.Id);
                result.Data = mapper.Map<ContactUsDto>(ContactUs);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteContactUs(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var ContactUs = await unit.ContactUs.GetWithDetailAsync(p => p.Id == dto.Id);
                if (ContactUs== null )
      
                {
                    result.Error(ErrorCodes.EntityNotFound);
                    return result;
                }
                unit.ContactUs.Remove(ContactUs);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiPageResult<ContactUsDto>> GetContactUsByFilter(GetContactUsByFilterDto dto)
        {
            var result = new ApiPageResult<ContactUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var query = unit.ContactUs.GetByFilter(dto);
                var paged = new Paged<ContactUs, ContactUsDto>(query, dto, mapper.Map<ContactUsDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
              
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
    }


}
