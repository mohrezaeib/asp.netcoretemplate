﻿
using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IContactUsCRUD : IApplicationService
    {
        Task<ApiResult<ContactUsDto>> EditContactUsStatus(EditContactUsStatusDto dto);
        Task<ApiResult<ContactUsDto>> AddContactUs(AddContactUsDto dto);
        Task<BaseApiResult> DeleteContactUs(BaseByIntDto dto);
        Task<ApiPageResult<ContactUsDto>> GetContactUsByFilter(GetContactUsByFilterDto dto);





    }

}
