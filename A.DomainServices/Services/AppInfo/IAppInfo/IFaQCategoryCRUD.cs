﻿using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IFaQCategoryCRUD : IApplicationService
    {
        Task<ApiResult<FaQCategoryShortDto>> EditFaQCategory(EditFaQCategoryDto dto);
        Task<ApiResult<FaQCategoryShortDto>> AddFaQCategory(AddFaQCategoryDto dto);
        Task<BaseApiResult> DeleteFaQCategory(BaseByIntDto dto);
        Task<ApiListResult<FaQCategoryShortDto>> GetAllFaQCategory( );

        Task<ApiListResult<FaQCategoryDto>> GetAllFaQCategoryWithDetail();



    }

}
