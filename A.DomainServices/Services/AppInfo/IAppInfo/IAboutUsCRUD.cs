﻿
using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;

using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IAboutUsCRUD : IApplicationService
    {
        Task<ApiResult<AboutUsDto>> EditAboutUs(EditAboutUsDto dto);
       // Task<ApiResult<AboutUsDto>> AddAboutUs(AddAboutUsDto dto);
       //Task<BaseApiResult> DeleteAboutUs(BaseByIntDto dto);
        Task<ApiResult<AboutUsDto>> GetAboutUs( );


       

    }

}
