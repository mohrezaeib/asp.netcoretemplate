﻿
using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ISocialCRUD : IApplicationService
    {
        Task<ApiResult<SocialDto>> EditSocial(EditSocialDto dto);
        Task<ApiListResult<SocialDto>> EditSocialByList(List<EditSocialDto> dto);
        Task<ApiResult<SocialDto>> AddSocial(AddSocialDto dto);
        Task<BaseApiResult> DeleteSocial(BaseByIntDto dto);
        Task<ApiListResult<SocialDto>> GetAllSocial( );


       

    }

}
