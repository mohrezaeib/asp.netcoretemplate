﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class SocialCRUD : ISocialCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public SocialCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<SocialDto>> EditSocial(EditSocialDto dto)
        {
            var result = new ApiResult<SocialDto> { Status = true, Message = Messages.EngOK };
            try
            {

                var social = await unit.Social.GetWithDetailAsync(p => p.Id == dto.Id);
                social = mapper.Map<EditSocialDto, Social>(dto, social);

                social.AboutUsId = unit.AboutUs.GetAll()?.FirstOrDefault()?.Id;

                await unit.CompleteAsync();
                social = await unit.Social.GetWithDetailAsync(p => p.Id == social.Id);
                result.Data = mapper.Map<SocialDto>(social);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiListResult<SocialDto>> EditSocialByList(List<EditSocialDto> dto)
        {
            var result = new ApiListResult<SocialDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resultList = new List<SocialDto>();
                foreach (var d in dto)
                {
                    var social = await unit.Social.GetWithDetailAsync(p => p.Id == d.Id);
                    social = mapper.Map<EditSocialDto, Social>(d, social);

                    social.AboutUsId = unit.AboutUs.GetAll()?.FirstOrDefault()?.Id;

                    await unit.CompleteAsync();
                    social = await unit.Social.GetWithDetailAsync(p => p.Id == social.Id);
                    resultList.Add(mapper.Map<SocialDto>(social));
                }
                result.Data = resultList;
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<SocialDto>> AddSocial(AddSocialDto dto)
        {
            var result = new ApiResult<SocialDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var social = mapper.Map<Social>(dto);
                social.AboutUsId = unit.AboutUs.GetAll()?.FirstOrDefault()?.Id;
                await unit.Social.AddAsync(social);
                await unit.CompleteAsync();
                social = await unit.Social.GetWithDetailAsync(p => p.Id == social.Id);
                result.Data = mapper.Map<SocialDto>(social);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteSocial(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var Social = await unit.Social.GetWithDetailAsync(p => p.Id == dto.Id);
                if (Social == null)

                {
                    result.Error(ErrorCodes.EntityNotFound);
                    return result;
                }
                unit.Social.Remove(Social);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiListResult<SocialDto>> GetAllSocial()
        {
            var result = new ApiListResult<SocialDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.Social.GetAllWithDetailAsync();
                result.Data = resp.Select(p => mapper.Map<SocialDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


    }


}
