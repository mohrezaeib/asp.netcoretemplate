﻿using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;
using DomainCore.Entities.AppMore;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class FaQCategoryCRUD : IFaQCategoryCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public FaQCategoryCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<FaQCategoryShortDto>> EditFaQCategory(EditFaQCategoryDto dto)
        {
            var result = new ApiResult<FaQCategoryShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQCategory = await unit.FaQCategory.GetWithDetailAsync(p => p.Id == dto.Id);
                FaQCategory = mapper.Map<EditFaQCategoryDto, FaQCategory>(dto, FaQCategory);
                await unit.CompleteAsync();
                FaQCategory = await unit.FaQCategory.GetWithDetailAsync(p => p.Id == FaQCategory.Id);
                result.Data = mapper.Map< FaQCategoryShortDto>( FaQCategory);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<FaQCategoryShortDto>> AddFaQCategory(AddFaQCategoryDto dto)
        {
            var result = new ApiResult<FaQCategoryShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQCategory = mapper.Map< FaQCategory>(dto);
                await unit.FaQCategory.AddAsync(FaQCategory);
                await unit.CompleteAsync();
                FaQCategory = await unit.FaQCategory.GetWithDetailAsync(p => p.Id == FaQCategory.Id);
                result.Data = mapper.Map<FaQCategoryShortDto>(FaQCategory);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteFaQCategory(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var FaQCategory = await unit.FaQCategory.GetWithDetailAsync(p => p.Id == dto.Id);
                if (FaQCategory== null )
      
                {
                    result.Error(ErrorCodes.EntityNotFound);
                    return result;
                }
                unit.FaQCategory.Remove(FaQCategory);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiListResult<FaQCategoryShortDto>> GetAllFaQCategory()
        {
            var result = new ApiListResult<FaQCategoryShortDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.FaQCategory.GetAllWithDetailAsync();
                result.Data = resp.Select(p=> mapper.Map<FaQCategoryShortDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
         public async Task<ApiListResult<FaQCategoryDto>> GetAllFaQCategoryWithDetail()
        {
            var result = new ApiListResult<FaQCategoryDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.FaQCategory.GetAllWithDetailAsync();
                result.Data = resp.Select(p=> mapper.Map<FaQCategoryDto>(p)).ToList();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
    }


}
