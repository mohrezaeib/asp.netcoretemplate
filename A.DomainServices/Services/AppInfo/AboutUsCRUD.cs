﻿
using AutoMapper;
using DomainCore.Dto.Base;

using DomainCore.DTO.AppMore;
using DomainCore.DTO.Base;

using DomainCore.Entities.AppMore;

using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;

using System;



using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class AboutUsCRUD : IAboutUsCRUD
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;

        public AboutUsCRUD(IUnitOfWork unit,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.mapper = mapper;
        }
        public async Task<ApiResult<AboutUsDto>> EditAboutUs(EditAboutUsDto dto)
        {
            var result = new ApiResult<AboutUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var AboutUs = await unit.AboutUs.GetWithDetailAsync(p => p.Id == dto.Id);
                AboutUs = mapper.Map<EditAboutUsDto, AboutUs>(dto, AboutUs);
                await unit.CompleteAsync();
                AboutUs = await unit.AboutUs.GetWithDetailAsync(p => p.Id == AboutUs.Id);
                result.Data = mapper.Map< AboutUsDto>( AboutUs);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<AboutUsDto>> AddAboutUs(AddAboutUsDto dto)
        {
            var result = new ApiResult<AboutUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var AboutUs = mapper.Map< AboutUs>(dto);
                await unit.AboutUs.AddAsync(AboutUs);
                await unit.CompleteAsync();
                AboutUs = await unit.AboutUs.GetWithDetailAsync(p => p.Id == AboutUs.Id);
                result.Data = mapper.Map<AboutUsDto>(AboutUs);


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteAboutUs(BaseByIntDto dto)
        {
            var result = new BaseApiResult { Status = true, Message = Messages.EngOK };
            try
            {
                var AboutUs = await unit.AboutUs.GetWithDetailAsync(p => p.Id == dto.Id);

                if (AboutUs== null )
      
                {
                    result.Error(ErrorCodes.EntityNotFound);
                    return result;
                }
                unit.Social.RemoveRange(AboutUs.Socials);

                unit.AboutUs.Remove(AboutUs);

                await unit.CompleteAsync();



            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }


        public async Task<ApiResult<AboutUsDto>> GetAboutUs()
        {
            var result = new ApiResult<AboutUsDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var resp = await unit.AboutUs.GetAllWithDetailAsync();
                result.Data = resp.Any()?  mapper.Map<AboutUsDto>(resp.FirstOrDefault()) : null;
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        
    }


}
