﻿using AutoMapper;
using DomainCore.Dto.Base;
using DomainData.Interfaces;
using System;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IApplicationService
    {
    }
    public interface IApplicationService<T,ReadDto,AddDto,EditDto,FilterPageDto, IdDto> where  T : class 
    {
        Task<ApiResult<ReadDto>> Add(AddDto dto);
        Task<ApiResult<ReadDto>> Edit(EditDto dto);
        Task<BaseApiResult> Delete(IdDto dto);
        Task<ApiPageResult<ReadDto>> GetByFilter(FilterPageDto dto);
        Task<ApiResult<ReadDto>> GetById(IdDto dto);
    }

    public class ApplicationService<T, ReadDto, AddDto, EditDto, FilterPageDto, IdDto> : IApplicationService<T, ReadDto, AddDto, EditDto, FilterPageDto, IdDto> where T : class
    {
        private readonly IRepository<T> repo;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unit;

        public ApplicationService(IRepository<T> repo , IMapper mapper , IUnitOfWork unit)
        {
            this.repo = repo;
            this.mapper = mapper;
            this.unit = unit;
        }
        public async Task<ApiResult<ReadDto>> Add(AddDto dto)
        {
            var result = new ApiResult<ReadDto>();
            try
            {
                var x = mapper.Map<T>(dto);
                await repo.AddAsync(x);
                unit.Complete();
                result.Data = mapper.Map<ReadDto>(x);


            }
            catch(Exception e)
            {
                result.Error(e);
                return result;
            }

            return result;
        }
        public async Task<ApiResult<ReadDto>> Edit(EditDto dto)
        {
            var result = new ApiResult<ReadDto>();
            try
            {
                //var x = await repo.GetAllWithDetailAsync(dto);
                //await repo.AddAsync(x);
                //unit.Complete();
                //result.Data = mapper.Map<ReadDto>(x);

            }
            catch (Exception e)
            {
                result.Error(e);
                return result;
            }

            return result;
        }
        public async Task<BaseApiResult> Delete(IdDto dto)
        {
            var result = new BaseApiResult();
            try
            {

            }
            catch (Exception e)
            {
                result.Error(e);
                return result;
            }

            return result;
        }
        public async Task<ApiPageResult<ReadDto>> GetByFilter(FilterPageDto dto)
        {
            var result = new ApiPageResult<ReadDto>();
            try
            {

            }
            catch (Exception e)
            {
                result.Error(e);
                return result;
            }

            return result;
        }
        public async Task<ApiResult<ReadDto>> GetById(IdDto dto)
        {
            var result = new ApiResult<ReadDto>();
            try
            {

            }
            catch (Exception e)
            {
                result.Error(e);
                return result;
            }

            return result;
        }
    }
}
