﻿
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.SupportChat;
using DomainCore.Entities;
using DomainCore.Entities.SupportChat;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class CustomerSupportChat : ICustomerSupportChat
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public CustomerSupportChat(IUnitOfWork unit,

           IMapper mapper,
           ICurrentUser currentUser
            )
        {
            this.unit = unit;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }
        public async Task<ApiResult<Message>> SendMessage(SendMessageDto dto)
        {
            var result = new ApiResult<Message> { Status = true, Message = Messages.EngOK };
            try
            {
                var message = mapper.Map<Message>(dto);
                await unit.Message.AddAsync(message);
                await unit.CompleteAsync();
                message = await unit.Message.GetWithDetailAsync(p => p.Id == message.Id);

                result.Data = message;
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<StartSupportChatDto>> StartSupportChat()
        {
            var result = new ApiResult<StartSupportChatDto> { Status = true, Message = Messages.EngOK };
            result.Data = new StartSupportChatDto();
            try
            {

                var chat =await  unit.Chat.GetWithDetailAsync(p => p.CustomerId == currentUser.UserId);
                if (chat == null) {
                    chat = new Chat() { 
                    CustomerId = currentUser.UserId.Value,
                    };
                    await unit.Chat.AddAsync(chat);
                    await unit.CompleteAsync();

                }


                result.Data.Chat = mapper.Map<ChatDto>(chat);
            }
            catch (Exception e)
            {
                result.Error(e);

            }


            return result;
        }
    }


}
