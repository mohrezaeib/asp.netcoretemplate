﻿
using DomainCore.Dto.Base;

using DomainCore.DTO.SupportChat;
using DomainCore.Entities.SupportChat;

using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ICustomerSupportChat : IApplicationService
    {
        Task<ApiResult<StartSupportChatDto>> StartSupportChat();
       // Task<ApiResult<AboutUsDto>> AddAboutUs(AddAboutUsDto dto);
       //Task<BaseApiResult> DeleteAboutUs(BaseByIntDto dto);
        Task<ApiResult<Message>> SendMessage(SendMessageDto dto );


       

    }

}
