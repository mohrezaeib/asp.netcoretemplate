﻿
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.SupportChat;
using DomainCore.Entities;
using DomainCore.Entities.SupportChat;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class AdminSupportChat : IAdminSupportChat
    {
        private readonly IUnitOfWork unit;
        private readonly ICurrentUser currentUser;
        private readonly IMapper mapper;

        public AdminSupportChat(IUnitOfWork unit,
                       ICurrentUser currentUser,

           IMapper mapper
            )
        {
            this.unit = unit;
            this.currentUser = currentUser;
            this.mapper = mapper;
        }

        public async Task<ApiResult<Message>> SendMessage(SendMessageDto dto)
        {
            var result = new ApiResult<Message> { Status = true, Message = Messages.EngOK };
            try
            {
                var message = mapper.Map<Message>(dto);
                await unit.Message.AddAsync(message);
                await unit.CompleteAsync();
                message = await unit.Message.GetWithDetailAsync(p => p.Id == message.Id);
                result.Data = (message);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiPageResult<ChatDto>> GetChatByFilter(GetChatByFilterDto dto)
        {
            var result = new ApiPageResult<ChatDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var query = unit.Chat.GetByFilter(dto);

                var paged = new Paged<Chat, ChatDto>(query, dto, mapper.Map<ChatDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<Chat>> MarkChatAsRead(BaseByGuidDto dto)

        {
            var result = new ApiResult<Chat> { Status = true, Message = Messages.EngOK };
            try
            {
                var chat = await unit.Chat.GetWithDetailAsync(p=> p.Id== dto.Id);
                chat.Messages.ForEach(p => p.MessageStatus = MessageStatus.Read);
                await unit.CompleteAsync();
                result.Data = chat;


            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }
      
        public async Task AddUsersToGroup(IEnumerable<string> connections, string groupName, IGroupManager groups)
        {
            //connections.ToList().ForEach(id => {
            //    groups.AddToGroupAsync(id, groupName);

            //});
            foreach (var id in connections)
            {
                await groups.AddToGroupAsync(id, groupName);

            }
        }

    }





}
