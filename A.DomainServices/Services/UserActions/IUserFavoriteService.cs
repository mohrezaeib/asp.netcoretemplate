﻿using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface IUserFavoriteService : IApplicationService
    {

        Task<ApiResult<UserFavoriteDto>> AddUserFavoriteByUser(AddUserFavoriteDto dto);
        Task<BaseApiResult> DeleteUserFavorite(AddUserFavoriteDto dto);
        Task<ApiResult<UserFavoriteDto>> GetUserFavoriteById(BaseByGuidDto dto);
        Task<ApiResult<UserFavoriteListDto>> GetUserFavoritesByUserId(BaseByGuidDto dto);


    }

}
