﻿
using Atlantis.DomainServices.Mapping;
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;
using DomainCore.Entities.UserActions;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Tools.General;


namespace DomainServices.Services
{
    public class UserFavoriteService : IUserFavoriteService
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;
  

        public UserFavoriteService(
            IUnitOfWork unit,
            IMapper mapper 

            )
        {
            this.unit = unit;
            this.mapper = mapper;
 
        }
        public async Task<ApiResult<UserFavoriteDto>> AddUserFavoriteByUser(AddUserFavoriteDto dto)
        {
            var result = new ApiResult<UserFavoriteDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var exist = await unit.UserFavorite.IsExistAsync(dto);
                if(exist.Count == 0)
                {
                    var UserFavorite = mapper.Map<UserFavorite>(dto);
                    await unit.UserFavorite.AddAsync(UserFavorite);
                    unit.Complete();
                    UserFavorite = await unit.UserFavorite.GetWithDetailAsync(p => p.Id == UserFavorite.Id);
                    result.Data = mapper.Map<UserFavoriteDto>(UserFavorite);
                }
                else
                {
                    result.Message = "This Favorite Already Exist.";
                }
               

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteUserFavorite(AddUserFavoriteDto dto)
        {
            var result = new ApiResult<string> { Status = true, Message = Messages.EngOK };
            try
            {
                var UserFavorite = await unit.UserFavorite.IsExistAsync(dto);

                unit.UserFavorite.RemoveRange(UserFavorite);

                try
                {
                    await unit.CompleteAsync();

                }
                catch (Exception e)
                {
                    result.Error(ErrorCodes.EntityCouldNotDeleted);
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<UserFavoriteDto>> GetUserFavoriteById(BaseByGuidDto dto)
        {
            var result = new ApiResult<UserFavoriteDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var cl = await unit.UserFavorite.GetWithDetailAsync(p => p.Id == dto.Id);
                result.Data = mapper.Map<UserFavoriteDto>(cl);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiResult<UserFavoriteListDto>> GetUserFavoritesByUserId(BaseByGuidDto dto)
        {
            var result = new ApiResult<UserFavoriteListDto> { Status = true, Message = Messages.EngOK };
            try
            {
                result.Data = new UserFavoriteListDto();
                 var data =await  unit.UserFavorite.GetByUserId(dto);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

       
    }
}
