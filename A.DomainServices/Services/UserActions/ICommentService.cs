﻿using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;

using System.Threading.Tasks;

namespace DomainServices.Contracts
{
    public interface ICommentService : IApplicationService
    {

        Task<ApiResult<CommenDtoForMobile>> AddCommentByUser(AddCommentDto dto);
        Task<ApiResult<CommentBigDto>> AddAnswerByAdmin(AddCommentAnswerDto dto);
        Task<BaseApiResult> DeleteComment(BaseByGuidDto dto);
        Task<ApiResult<CommentBigDto>> EditComment(EditCommentDto dto);
        Task<ApiResult<CommentBigDto>> GetCommentById(BaseByGuidDto dto);
        Task<ApiPageResult<CommentBigDto>> GetCommentsByFilter(GetCommentByFiltrerDto dto);



    }

}
