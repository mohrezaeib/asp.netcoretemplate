﻿
using AutoMapper;
using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;
using DomainCore.Entities.UserActions;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainServices.Contracts;
using System;
using System.Collections.Generic;

using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainServices.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork unit;
        private readonly IMapper mapper;
  

        public CommentService(
            IUnitOfWork unit,
            IMapper mapper 

            )
        {
            this.unit = unit;
            this.mapper = mapper;
 
        }
        public async Task<ApiResult<CommenDtoForMobile>> AddCommentByUser(AddCommentDto dto)
        {
            var result = new ApiResult<CommenDtoForMobile> { Status = true, Message = Messages.EngOK };
            try
            {
                var Comment = mapper.Map<Comment>(dto);
                Comment.IsAnswered = false;
                await unit.Comment.AddAsync(Comment);
                unit.Complete();
                Comment =  await unit.Comment.GetWithDetailAsync(p=> p.Id == Comment.Id);
                result.Data  = mapper.Map<CommenDtoForMobile>(Comment);

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }
        public async Task<ApiResult<CommentBigDto>> AddAnswerByAdmin(AddCommentAnswerDto dto)
        {
            var result = new ApiResult<CommentBigDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var Comment = mapper.Map<Comment>(dto);
                Comment.IsFromAdmin = true;
                var parentComment = await unit.Comment.GetWithDetailAsync(p => p.Id == dto.ParentId);
                if (parentComment != null) { parentComment.IsAnswered = true; parentComment.Status = Status.Active; }
                else { result.Error(ErrorCodes.EntityNotFound); return result; }

                await unit.Comment.AddAsync(Comment);
                unit.Complete();
                Comment = await unit.Comment.GetWithDetailAsync(p => p.Id == Comment.Id);
                result.Data = mapper.Map<CommentBigDto>(Comment);

            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<BaseApiResult> DeleteComment(BaseByGuidDto dto)
        {
            var result = new ApiResult<string> { Status = true, Message = Messages.EngOK };
            try
            {
                var Comment = await unit.Comment.GetWithDetailAsync(p => p.Id == dto.Id);

                unit.Comment.Remove(Comment);

                try
                {
                    await unit.CompleteAsync();

                }
                catch (Exception e)
                {
                    result.Error(ErrorCodes.EntityCouldNotDeleted);
                }
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;

        }

        public async Task<ApiResult<CommentBigDto>> EditComment(EditCommentDto dto)
        {
            var result = new ApiResult<CommentBigDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var comment = await unit.Comment.GetWithDetailAsync(p => p.Id == dto.Id);
                if (comment == null) { result.Error(ErrorCodes.EntityNotFound); return result; }
             
                comment = mapper.Map<EditCommentDto, Comment>(dto , comment);
                await unit.CompleteAsync();
                comment = await unit.Comment.GetWithDetailAsync(p => p.Id == comment.Id);
                result.Data = mapper.Map<CommentBigDto>(comment);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        

     
        public async Task<ApiResult<CommentBigDto>> GetCommentById(BaseByGuidDto dto)
        {
            var result = new ApiResult<CommentBigDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var cl = await unit.Comment.GetWithDetailAsync(p => p.Id == dto.Id);
                result.Data = mapper.Map<CommentBigDto>(cl);
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

        public async Task<ApiPageResult<CommentBigDto>> GetCommentsByFilter(GetCommentByFiltrerDto dto)
        {
            var result = new ApiPageResult<CommentBigDto> { Status = true, Message = Messages.EngOK };
            try
            {
                var query = unit.Comment.GetByFilter(dto);
                var paged = new Paged<Comment, CommentBigDto>(query, dto, mapper.Map<CommentBigDto>);

                result.Data = await paged.GetItmesAsync();
                result.PageDto = paged.GetPageDto();
     
            }
            catch (Exception e)
            {
                result.Error(e);
            }

            return result;
        }

       
    }
}
