﻿using DomainServices.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using AutoMapper;
using DomainServices.DtoService.Profiles;

using AutoMapper.EquivalencyExpression;



namespace Atlantis.DomainServices
{
    public static class Extentions
    {

        public static void AddApplicationServices(this IServiceCollection services)
        {
            var applicationServiceType = typeof(IApplicationService).Assembly;
            applicationServiceType.ExportedTypes.ToList().ForEach(p => Console.WriteLine(p.FullName));
             var AllApplicationServices = applicationServiceType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("DomainServices.Services")  ).ToList();
            foreach (var type in AllApplicationServices)
            {
                Console.WriteLine(type.ToString());
                Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }

        public static IServiceCollection AddAutoMapperExtention(this IServiceCollection servicess)
        {
            var services = servicess;
            services.AddEntityFrameworkInMemoryDatabase();
                
            services.AddAutoMapper((srp, cfg) =>
            {
                cfg.AddCollectionMappers();

                // cfg.UseEntityFrameworkCoreModel<MainContext>(services);
            }, typeof(UserProfile).Assembly);
            var serviceProvider = services.BuildServiceProvider();



            //Validate configs 

            var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddCollectionMappers();
            
                    //cfg.AddProfile(typeof(GeneralProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(ClinicProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(ServiceProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(BodyPartProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(PackageProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(CommentProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(UserFavoriteProfile)); // <- you can do this however you like
                    //cfg.AddProfile(typeof(UserProfile)); // <- you can do this however you like
                    //cfg.AllowNullDestinationValues = true;


                });
            //config.AssertConfigurationIsValid();

            return servicess;
        }
    }
}
