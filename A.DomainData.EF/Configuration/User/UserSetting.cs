﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class UserSetting : IEntityTypeConfiguration<DomainCore.Entities.UserSetting>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.UserSetting> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasOne(p => p.User).WithOne(p => p.UserSetting)
                .HasForeignKey<DomainCore.Entities.UserSetting>(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
        }
    }

}