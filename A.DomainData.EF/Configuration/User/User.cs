﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class User : IEntityTypeConfiguration<DomainCore.AppUsers.User>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppUsers.User> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.CreatedAt).IsRequired();
            //builder.Property(p => p.Password).IsRequired();
            //builder.Property(p => p.Salt).IsRequired();
            builder.Property(p => p.Status).IsRequired();
            //builder.Property(p => p.Email).IsRequired();
            // builder.HasOne(p => p.ProfileImage).WithOne(p=>p.Us).HasForeignKey(p => p.ProfileImageId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.Reference).WithMany().HasForeignKey(p => p.ReferenceId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.UserSetting).WithOne(p => p.User)
                .HasForeignKey<DomainCore.AppUsers.User>(p => p.UserSettingId).OnDelete(DeleteBehavior.Restrict);

        }
    }

}