﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Transaction : IEntityTypeConfiguration<DomainCore.AppTransactions.Transaction>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppTransactions.Transaction> builder)
        //{
        //    builder.HasKey(p => new { p.Id });
        //    builder.HasOne(p => p.User).WithMany(p=>p.Transaction).HasForeignKey(p=>p.UserId).OnDelete(DeleteBehavior.Restrict);
        //    builder.HasOne(p => p.NextUser).WithMany(p=>p.OtherTransaction).HasForeignKey(p=>p.NextUserId).OnDelete(DeleteBehavior.Restrict);
        //}
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

        }
    }
}