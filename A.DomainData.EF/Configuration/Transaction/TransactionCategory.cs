﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class TransactionCategory : IEntityTypeConfiguration<DomainCore.AppTransactions.TransactionCategory>
    {
        public void Configure(EntityTypeBuilder<DomainCore.AppTransactions.TransactionCategory> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.Id).ValueGeneratedNever();
        }
    }
}