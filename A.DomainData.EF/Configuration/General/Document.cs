﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Ef.Configuration
{
    public class Document : IEntityTypeConfiguration<DomainCore.General.Document>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.Document> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.CreatedAt).IsRequired();
            builder.Property(p => p.DocumentType).IsRequired();
            builder.Property(p => p.Location).IsRequired();
        }
    }
}
