﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Ef.Configuration
{
    public class Update : IEntityTypeConfiguration<DomainCore.General.Update>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.Update> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

        }
    }
}
