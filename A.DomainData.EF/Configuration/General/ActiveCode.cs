﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;


namespace DomainData.Ef.Configuration
{
    public class ActiveCode : IEntityTypeConfiguration<DomainCore.General.ActiveCode>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.ActiveCode> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.Code).IsRequired().HasMaxLength(10);
            builder.Property(p => p.Mobile).IsRequired();
            builder.Property(p => p.CreatedAt).IsRequired();
 
        }
    }
}