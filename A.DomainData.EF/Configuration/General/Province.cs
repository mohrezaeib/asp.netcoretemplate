﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Province : IEntityTypeConfiguration<DomainCore.General.Province>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.Province> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.Id).ValueGeneratedNever();
            builder.Property(p => p.Name).IsRequired();
            builder.HasOne(p => p.Country).WithMany(p => p.Province).HasForeignKey(p => p.CountryId).OnDelete(DeleteBehavior.Cascade);

        }
    }
}