﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Device : IEntityTypeConfiguration<DomainCore.General.Device>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.Device> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.PushId).IsRequired();
            builder.Property(p => p.CreatedAt).IsRequired();
            builder.HasOne(p=>p.User).WithMany(b => b.Device).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}