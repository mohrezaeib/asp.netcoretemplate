﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class City : IEntityTypeConfiguration<DomainCore.General.City>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.City> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.Id).ValueGeneratedNever();
            builder.Property(p => p.Name).IsRequired();
            builder.HasOne(p => p.Province).WithMany(p=>p.Cities).HasForeignKey(p => p.ProvinceId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}