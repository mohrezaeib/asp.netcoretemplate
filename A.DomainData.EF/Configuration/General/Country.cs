﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Country : IEntityTypeConfiguration<DomainCore.General.Country>
    {
        public void Configure(EntityTypeBuilder<DomainCore.General.Country> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.Property(p => p.Id).ValueGeneratedNever();
            builder.Property(p => p.Name).IsRequired();
        }
    }
}