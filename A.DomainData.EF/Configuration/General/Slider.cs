﻿using DomainCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DomainData.Ef.Configuration
{
    public class Slider : IEntityTypeConfiguration<DomainCore.Entities.General.Slider>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.General.Slider> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.HasOne(p => p.Document).WithMany(p => p.Sliders)
                .HasForeignKey(p => p.DocumentId).OnDelete(DeleteBehavior.Restrict);


        }
    } 
}
