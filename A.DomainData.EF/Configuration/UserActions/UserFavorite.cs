﻿using DomainCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DomainData.Ef.Configuration
{
    public class UserFavorite : IEntityTypeConfiguration<DomainCore.Entities.UserActions.UserFavorite>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.UserActions.UserFavorite> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.HasOne(p => p.User).WithMany(p => p.UserFavorite)
                 .HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.Cascade);

           

        }
    }
}
