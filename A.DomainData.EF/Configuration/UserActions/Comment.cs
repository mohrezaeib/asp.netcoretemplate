﻿using DomainCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DomainData.Ef.Configuration
{
    public class Comment : IEntityTypeConfiguration<DomainCore.Entities.UserActions.Comment>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.UserActions.Comment> builder)
        {
            builder.HasKey(p => new { p.Id });
            builder.HasQueryFilter(m => !m.isDeleted);

            builder.HasOne(p => p.Parent).WithMany(p => p.Comments)
       .HasForeignKey(p => p.ParentId).OnDelete(DeleteBehavior.Restrict);   
            builder.HasOne(p => p.Sender).WithMany(p => p.Comments)
       .HasForeignKey(p => p.SenderId).OnDelete(DeleteBehavior.Restrict);

          
   
        }
    }
}
