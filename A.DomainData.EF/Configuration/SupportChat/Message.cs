﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Message : IEntityTypeConfiguration<DomainCore.Entities.SupportChat.Message>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.SupportChat.Message> builder)
        {
            builder.HasKey(p => new { p.Id });

            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasOne(p => p.Sender).WithMany(p=> p.Messages).HasForeignKey(p => p.SenderId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.Chat).WithMany(p=> p.Messages).HasForeignKey(p => p.ChatId).OnDelete(DeleteBehavior.Cascade);


        }
    }
}
