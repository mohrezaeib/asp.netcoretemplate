﻿using DomainCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DomainData.Ef.Configuration
{
    public class Chat : IEntityTypeConfiguration<DomainCore.Entities.SupportChat.Chat>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.SupportChat.Chat> builder)
        {
            builder.HasKey(p => new { p.Id });
        
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasOne(p => p.Customer).WithOne(p=> p.SupportChat).HasForeignKey<DomainCore.Entities.SupportChat.Chat>(p=> p.CustomerId).OnDelete(DeleteBehavior.Restrict);
           builder.HasMany(p => p.Messages).WithOne( p=> p.Chat).HasForeignKey(p=> p.ChatId).OnDelete(DeleteBehavior.Cascade);
            


        }
    }
}
