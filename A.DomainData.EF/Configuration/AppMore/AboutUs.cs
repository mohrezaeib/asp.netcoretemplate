﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class AboutUs : IEntityTypeConfiguration<DomainCore.Entities.AppMore.AboutUs>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.AppMore.AboutUs> builder)
        {
            builder.HasKey(p => new { p.Id });
            //builder.Property<bool>("isDeleted");
            //builder.HasQueryFilter(m => EF.Property<bool>(m, "isDeleted"));
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasMany(p => p.Socials).WithOne(p => p.AboutUs)
                .HasForeignKey(p => p.AboutUsId).OnDelete(DeleteBehavior.Cascade);



        }
    }
    public class FaQ : IEntityTypeConfiguration<DomainCore.Entities.AppMore.FaQ>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.AppMore.FaQ> builder)
        {
            builder.HasKey(p => new { p.Id });
            //builder.Property<bool>("isDeleted");
            //builder.HasQueryFilter(m => EF.Property<bool>(m, "isDeleted"));
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasOne(p => p.Category).WithMany(p => p.FaQs)
                .HasForeignKey(p => p.CategoryId).OnDelete(DeleteBehavior.Restrict);



        }
    }
}
