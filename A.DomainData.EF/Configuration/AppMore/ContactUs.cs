﻿using DomainCore.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace DomainData.Ef.Configuration
{
    public class ContactUs : IEntityTypeConfiguration<DomainCore.Entities.AppMore.ContactUs>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.AppMore.ContactUs> builder)
        {
            builder.HasKey(p => new { p.Id });
            //builder.Property<bool>("isDeleted");
            //builder.HasQueryFilter(m => EF.Property<bool>(m, "isDeleted"));
            builder.HasQueryFilter(m => !m.isDeleted);
            builder.HasOne(p => p.User).WithMany().HasForeignKey(prop => prop.UserId).OnDelete(DeleteBehavior.Cascade);

        }
    }
}
