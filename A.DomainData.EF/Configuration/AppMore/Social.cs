﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DomainData.Ef.Configuration
{
    public class Social : IEntityTypeConfiguration<DomainCore.Entities.AppMore.Social>
    {
        public void Configure(EntityTypeBuilder<DomainCore.Entities.AppMore.Social> builder)
        {
            builder.HasKey(p => new { p.Id });
            //builder.Property<bool>("isDeleted");
            //builder.HasQueryFilter(m => EF.Property<bool>(m, "isDeleted"));
            builder.HasQueryFilter(m => !m.isDeleted);



        }
    }
}
