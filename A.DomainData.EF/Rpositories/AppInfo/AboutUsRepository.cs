﻿using Core.Entities;
using DomainCore.Dto.General;
using DomainCore.Entities.AppMore;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class AboutUsRepository : Repository<AboutUs>, IAboutUsRepository
    {
        private readonly IContext ctx;

        public AboutUsRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            base.baseInclude = this.ctx.AboutUs.Include(p => p.Socials).ThenInclude(p=> p.Image);
        }
        


    } 
}
