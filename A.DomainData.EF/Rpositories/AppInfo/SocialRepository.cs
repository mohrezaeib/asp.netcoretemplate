﻿using DomainCore.Entities.AppMore;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;

namespace DomainData.Ef.Repositories
{
    public class SocialRepository : Repository<Social>, ISocialRepository
    {
        private readonly IContext ctx;

        public SocialRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.baseInclude = this.ctx.Socials.Include(p => p.Image);
        }
        


    } 
}
