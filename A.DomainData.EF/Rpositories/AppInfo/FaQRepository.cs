﻿using DomainCore.Entities.AppMore;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;

namespace DomainData.Ef.Repositories
{
    public class FaQRepository : Repository<FaQ>, IFaQRepository
    {
        private readonly IContext ctx;

        public FaQRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = this.ctx.FaQs.Include(prop => prop.Category);

        }



    } public class FaQCategoryRepository : Repository<FaQCategory>, IFaQCategoryRepository
    {
        private readonly IContext ctx;

        public FaQCategoryRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = this.ctx.FaQCategorys.Include(prop => prop.FaQs);
        }
        


    } 
}
