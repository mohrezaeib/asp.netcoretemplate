﻿using DomainCore.DTO.AppMore;
using DomainCore.Entities.AppMore;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DomainData.Ef.Repositories
{
    public class ContactUsRepository : Repository<ContactUs>, IContactUsRepository
    {
        private readonly IContext ctx;

        public ContactUsRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            base.baseInclude = this.ctx.ContactUs.Include(p => p.User.ProfileImage);
        }
        
        public IQueryable<ContactUs > GetByFilter(GetContactUsByFilterDto dto)
        {
            return baseInclude.Where(p =>
            (dto.Keyword == null || (p.Message != null &&  p.Message.Contains(dto.Keyword)))&&
            (dto.Status == null || (dto.Status == p.Status) )&&
            (dto.UserId == null || (dto.UserId == p.UserId) )&&
            (dto.From == null || (dto.From <= p.CreatedAt) )&&
            (dto.From == null || (dto.To >= p.CreatedAt))

            );
        }

    } 
}
