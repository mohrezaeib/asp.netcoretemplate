﻿using Core.Entities;
using DomainCore.Dto.General;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class UpdateRepository : Repository<Update>, IUpdateRepository
    {
        private readonly IContext ctx;

        public UpdateRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }
        public async Task<Update> GetUpdateAsync(GetSplashDto dto)
        {
            return await ctx.Updates.OrderBy(p=> p.CreatedAt).FirstOrDefaultAsync(p =>
                (p.FromVersionCode == null || p.FromVersionCode <= dto.VesionCode) &&
                (p.ToVersionCode == null || p.ToVersionCode >= dto.VesionCode) &&
                (p.OS == dto.OS) &&
                (p.PkgName == dto.PkgName)
                );
        }
    }
}
