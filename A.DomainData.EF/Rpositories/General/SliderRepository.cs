﻿

using DomainCore.Entities.General;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DomainData.Ef.Repositories
{
    public class SliderRepository : Repository<Slider>, ISliderRepository
    {
        private readonly IContext ctx;

        public SliderRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.baseInclude = ctx.Sliders.Include(p => p.Document);
        }

      
    }
}
