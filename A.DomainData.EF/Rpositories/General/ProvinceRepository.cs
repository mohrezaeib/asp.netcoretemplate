﻿
using DomainCore.Dto.General;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using EntityFrameworkCore.Cacheable;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class ProvinceRepository : Repository<Province>, IProvinceRepository
    {
        private readonly IContext ctx;

        public ProvinceRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

 

        public async Task<List<Province>> GetIncludeCtiesAsync()
        {
           return await ctx.Provinces.Include(p => p.Cities)
              //  .Cacheable(TimeSpan.FromDays(1))
                .ToListAsync();



        }
    }
}
