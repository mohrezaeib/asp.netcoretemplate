﻿

using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DomainData.Ef.Repositories
{
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        private readonly IContext ctx;

        public CountryRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

      
    }
}
