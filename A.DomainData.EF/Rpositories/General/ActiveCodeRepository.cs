﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using Utility.Tools.General;
using Core.Entities;
using DomainData.Interfaces;
using DomainCore.General;
using DomainData.Ef.Context;
using DomainCore.Dto.General;
using System.Linq.Expressions;

namespace DomainData.Ef.Repositories
{
    public class ActiveCodeRepository : Repository<ActiveCode>, IActiveCodeRepository
    {
        private readonly IContext ctx;

        public ActiveCodeRepository(IContext ctx) : base(ctx )
        {
            this.ctx = ctx;
        }

       

        public bool CheckExeed(string mobile)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            
            // var date = DateTime.Now.AddMinutes(-15).ToUnix();
            return ctx.ActiveCodes.Where(p => p.Mobile == mobile && p.CreatedAt > date1 ).ToList().Count() >= 5;
        }

        public bool CheckActiveCode(CheckActiveCodeDto dto)
        {
            long date1 = UnixTime.ToUnix(DateTime.Now.AddMinutes(-15));
            // my logic : return ctx.ActiveCodes.Any(p => p.Code == dto.ActiveCode && p.Mobile == dto.Mobile && p.CreatedAt > date1);
            var result = ctx.ActiveCodes.Where(p => p.Mobile == dto.Mobile && p.CreatedAt > date1).OrderByDescending(p => p.CreatedAt).ToList().FirstOrDefault();
            return result != null && result.Code == dto.Code;
        }

        List<ActiveCode> IRepository<ActiveCode>.Find(Expression<Func<ActiveCode, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
