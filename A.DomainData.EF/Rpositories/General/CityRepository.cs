﻿
using DomainData.Interfaces;
using DomainData.Ef.Context;
using DomainCore.Dto.General;
using DomainCore.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        private readonly IContext ctx;

        public CityRepository(IContext ctx) : base(ctx )
        {
            this.ctx = ctx;
        }

        public async Task<List<City>> GetByProvinceId(int provinceId)
        {
            return await ctx.Cities.Where(p => p.ProvinceId == provinceId).Include(p => p.Province).ToListAsync();
        }
    }
}

