﻿
using Core.Entities;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        private readonly IContext ctx;

        public DeviceRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        public bool IsExist(Guid userId, string deviceId)
        {
            return ctx.Devices.Any(d => d.UserId == userId && d.PushId == deviceId );
        }
        public List<Device> GetAllUserDevice(Guid userId)
        {
            return ctx.Devices.Where(d => d.UserId == userId ).ToList();
        }
      
        public async  Task<List<Device>> GetAllUserDeviceAsync(Guid userId)
        {
            return await ctx.Devices.Where(d => d.UserId == userId ).ToListAsync();
        }
        public async  Task<List<Device>> GetAllAsync()
        {
            return await ctx.Devices.ToListAsync();
        }
    }
}
