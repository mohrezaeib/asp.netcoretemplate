﻿using Core.Entities;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainData.Ef.Repositories
{
    public class DocumentRepository: Repository<Document>, IDocumentRepository
    {
        private readonly IContext ctx;

        public DocumentRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        public string GetUrl(Guid documentId)
        {
            return ctx.Documents.FirstOrDefault(p => p.Id == documentId).Location;
        }
    }
}
