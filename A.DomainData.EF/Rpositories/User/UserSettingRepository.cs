﻿using DomainCore.Entities;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DomainData.Ef.Repositories
{
    public partial class UserSettingRepository : Repository<UserSetting>, IUserSettingRepository
    {
        private readonly IContext ctx;
        private IQueryable<UserSetting> inclues;
        public UserSettingRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.inclues = ctx.UserSettings
                .Include(p => p.User);


        }
    }
    }

