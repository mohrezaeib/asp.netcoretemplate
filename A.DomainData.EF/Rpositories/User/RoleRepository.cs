﻿

using DomainCore.AppUsers;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainData.Ef.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        private readonly IContext ctx;

        public RoleRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }


    }
}
