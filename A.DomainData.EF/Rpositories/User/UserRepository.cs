﻿using DomainCore.AppUsers;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.General;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using DomainData.Ef.Extentions;
using MD.PersianDateTime.Standard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace DomainData.Ef.Repositories
{
    public partial class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IContext ctx;
        private  IQueryable<User> inclues;

        public UserRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            this.inclues = ctx.Users
                .Include(p => p.City).ThenInclude(p => p.Province)
                .Include(p => p.UserRole).ThenInclude(p => p.Role)
                .Include(p => p.ProfileImage)
                .Include(p => p.UserSetting)
                .Include(p => p.Documents).ThenInclude(p=> p.Document);


        }


        public async Task<User> GetByMobileAsync(string mobile)
        {
            return await inclues
                .Where(p => p.Mobile == mobile).FirstOrDefaultAsync();
        }

        public async Task<User> GetDetailAsync(Guid Id)
        {
            return await inclues
                            .Where(p => p.Id == Id).FirstOrDefaultAsync();
        }
        public IQueryable<User> GetByFilter(GetUsersByFilterDto dto)
        {
            var querry =  inclues.Where(p =>
                            (dto.CityId == null || dto.CityId == p.CityId) &&
                            (dto.Status == null  || dto.Status == p.Status) &&
                            (dto.Gender == null  || dto.Gender == p.Gender) &&
                             (dto.Date == null || dto.Date.Value.Date == p.CreatedAtDate.Date) &&
            (dto.FromDate == null || dto.FromDate.Value <= p.CreatedAtDate) &&
            (dto.ToDate == null || dto.ToDate.Value >= p.CreatedAtDate) &&
                            (
                            dto.Role == null ||dto.Role.Count==0|| p.UserRole.Select(r=> r)
                            .Any(rp => dto.Role.Contains(rp.RoleId) )
                            ) &&
                            (string.IsNullOrEmpty(dto.Keyword) || ((p.Name != null && p.Name.Contains(dto.Keyword))
                                                                  || (p.FamilyName != null && p.FamilyName.Contains(dto.Keyword))
                                                                  || (p.Email != null && p.Email.Contains(dto.Keyword))
                                                                  )) &&
                            (string.IsNullOrEmpty(dto.KeywordEmail) || (p.Email != null && p.Email.Contains(dto.KeywordEmail)))&&
                            (string.IsNullOrEmpty(dto.KeywordName) || ((p.Name != null && p.Name.Contains(dto.KeywordName)) || (p.FamilyName != null && p.FamilyName.Contains(dto.KeywordName))))&&
                            (string.IsNullOrEmpty(dto.KeywordPhone) || (p.Mobile != null && p.Mobile.Contains(dto.KeywordPhone))) &&
                            (string.IsNullOrEmpty(dto.KeywordFileNo) || (p.FileNo != null && p.FileNo.Contains(dto.KeywordFileNo))) 
                            ).OrderBy(p=> p.CreatedAt)
                ;
            if(dto.FileNoSorting!= 0)
            {
                querry = dto.FileNoSorting > 0 ? querry.OrderBy(p => p.FileNo) : querry.OrderByDescending(p => p.FileNo);
            }

            if (dto.NameSorting != 0)
            {
                querry = dto.NameSorting > 0 ? querry.OrderBy(p => p.FamilyName) : querry.OrderByDescending(p => p.FamilyName);
            }
            return querry;


        }

        public bool IsExist(string mobile)
        {
            return ctx.Users.Any(p => p.Mobile == mobile);
        }

        public async  Task<User> GetByEmailAsync(string email)
        {
            return await inclues.Where(p => p.Email == email).FirstOrDefaultAsync();
        }
    }
    }

