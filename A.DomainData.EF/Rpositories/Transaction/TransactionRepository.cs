﻿
using DomainCore.AppTransactions;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;


namespace DomainData.Ef.Repositories
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private IContext ctx;

        public TransactionRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        public int GetBalance(Guid userId)
        {
            return ctx.Transactions.Where(p => p.IsSuccessful && p.UserId == userId).Sum(p => p.Amount);
        }

       

        public List<Transaction> GetLastYear(long lastYear)
        {
            return ctx.Transactions.
               Where(p => p.CreatedAt >= DateTime.Now.AddYears(-1).ToUnix()).ToList();
        }

      

        public int GetTransactionsByPageCount(Guid userId)
        {
            return ctx.Transactions.
               Where(p => userId == p.UserId).Count();
        }

        public bool IsAuthorityExist(string authority)
        {
            var result =  ctx.Transactions.Any(p => p.Authority == authority);
            return result;
        }
        public Transaction GetByAuthority(string authority)
        {
            return ctx.Transactions.FirstOrDefault(p => p.Authority == authority);
        }
    }
}