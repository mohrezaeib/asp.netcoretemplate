﻿
using DomainCore.AppTransactions;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DomainData.Ef.Repositories
{
    public class TransactionCategoryRepository : Repository<TransactionCategory>, ITransactionCategoryRepository
    {
        private IContext ctx;

        public TransactionCategoryRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        
    }
}