﻿using DomainCore.DTO.SupportChat;
using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;
using DomainCore.Enums;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DomainData.Ef.Repositories
{
    public class ChatRepository : Repository<Chat>, IChatRepository
    {
        private readonly IContext ctx;

        public ChatRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = this.ctx.Chats
                .Include(p => p.Customer)
                .Include(p => p.Messages).ThenInclude(p => p.Sender)
                .Include(p => p.Messages).ThenInclude(p => p.Document);

        }
        public IQueryable<Chat> GetByFilter(GetChatByFilterDto dto)
        {
            return baseInclude.Where(p => (dto.ChatId == null || dto.ChatId == p.Id) &&
            (dto.HasUnSeenMessage == null || dto.HasUnSeenMessage ==
                                    p.Messages.Any(x => x.MessageStatus != MessageStatus.Read)
                                    ) &&
(dto.MessageDate == null || 
                                    p.Messages.Any(x => x.CreatedAtDate.Date == dto.MessageDate.Value.Date)
                                    )
                                    );
        }


    }  
}
