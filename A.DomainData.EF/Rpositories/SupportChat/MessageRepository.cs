﻿using DomainCore.Entities.SupportChat;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;

namespace DomainData.Ef.Repositories
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        private readonly IContext ctx;

        public MessageRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            baseInclude = this.ctx.Messages
                .Include(p=> p.Sender.ProfileImage)
                .Include(p=> p.Chat.Customer.ProfileImage);

        }



    } 
}
