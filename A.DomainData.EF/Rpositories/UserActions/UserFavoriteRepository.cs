﻿using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;
using DomainCore.Entities.UserActions;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class UserFavoriteRepository : Repository<UserFavorite>, IUserFavoriteRepository
    {
        private readonly IContext ctx;
        public UserFavoriteRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            base.baseInclude = this.ctx.UserFavorites
             
                .Include(p => p.User).ThenInclude(p => p.UserRole)
                .Include(p => p.User.ProfileImage)
              ;
        }

        public Task<List<UserFavorite>> GetByUserId(BaseByGuidDto dto)
        {
            return  baseInclude.Where(p => p.UserId == dto.Id).ToListAsync();
        }

        public   Task<List<UserFavorite>> IsExistAsync(AddUserFavoriteDto dto)
        {
            return   baseInclude.Where(p => (dto.UserId == p.UserId) ).ToListAsync();
        }
    }
}
