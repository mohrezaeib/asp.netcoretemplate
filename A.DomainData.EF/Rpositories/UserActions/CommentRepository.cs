﻿using DomainCore.DTO.UserActions;
using DomainCore.Entities.UserActions;
using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DomainData.Ef.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        private readonly IContext ctx;
        //  private readonly IQueryable<BodyPart> baseInclude;

        public CommentRepository(IContext ctx) : base(ctx)
        {
            this.ctx = ctx;
            base.baseInclude = this.ctx.Comments
                .Include(p => p.Comments)
                .Include(p => p.Sender).ThenInclude(p => p.UserRole)
                .Include(p => p.Sender).ThenInclude(p=>p.Documents)
                .Include(p => p.Sender.Documents).ThenInclude(p=> p.Document)
              ;
        }

        public IQueryable<Comment> GetByFilter(GetCommentByFiltrerDto dto)
        {
            return baseInclude.Where(p =>
                             (dto.SenderId == null || dto.SenderId == p.SenderId) &&
                             (dto.Status == null || dto.Status == p.Status) &&
                             (dto.IsAnswered == null || dto.IsAnswered == p.IsAnswered) &&
                             (dto.IsFromAdmin == null || dto.IsFromAdmin == p.IsFromAdmin) &&
                             (dto.From == null || dto.From <= p.CreatedAt) &&
                             (dto.To == null || dto.To >= p.CreatedAt) &&

                             (string.IsNullOrEmpty(dto.ContentKeyword) || ((p.Text != null && p.Text.Contains(dto.ContentKeyword)))) &&
                             (string.IsNullOrEmpty(dto.UserKeyword) || ((p.Sender.Name != null && p.Sender.Name.Contains(dto.UserKeyword)) || (p.Sender.FamilyName != null && p.Sender.FamilyName.Contains(dto.UserKeyword))))
                            );

                             
                             
        }
    }
}
