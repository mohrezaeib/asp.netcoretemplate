﻿
using DomainData.Interfaces;
using DomainCore.AppTransactions;
using DomainCore.AppUsers;
using DomainCore.General;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Transactions;
using DomainData.Ef.Extentions;
using System.Threading.Tasks;
using DomainCore.Entities.General;
using System.Threading;
using DomainCore.Base;
using Utility.Tools.General;
using DomainCore.Entities.UserActions;
using DomainCore.Entities;

using DomainCore.Entities.AppMore;

using DomainCore.Entities.SupportChat;
using System;


namespace DomainData.Ef.Context
{
    public class MainContext : DbContext, IContext
    {
        private readonly ICurrentUser sessionContext;

        public DbSet<User> Users { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Social> Socials { get; set; }
        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<FaQ> FaQs { get; set; }
        public DbSet<FaQCategory> FaQCategorys { get; set; }

        public DbSet<ActiveCode> ActiveCodes { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<PrivateDocument> PrivateDocuments { get; set; }
        public DbSet<DocumentUser> DocumentUsers { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Update> Updates { get; set; }

        public DbSet<DomainCore.AppTransactions.Transaction> Transactions { get; set; }
        public DbSet<TransactionCategory> TransactionCategories { get; set; }

        public DbSet<UserNotification> Notifications { get; set; }
  
        public DbSet<Slider> Sliders { get; set; }
     
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserFavorite> UserFavorites { get; set; }
     
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }

        public MainContext(DbContextOptions options , ICurrentUser sessionContext) : base(options)
        {
            this.sessionContext = sessionContext;
        }
     
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.AddEntityConfiguration();
            builder.SeedData();
            builder.GetCitiesFromJson();


        }


        void IContext.SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            this.SaveChanges();
        }

        async Task IContext.SaveChangesAsync()
        {
            UpdateSoftDeleteStatuses();
            await this.SaveChangesAsync();
        }
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get all the entities that inherit from AuditableEntity
            // and have a state of Added or Modified
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is AuditableEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            // For each entity we will set the Audit properties
            foreach (var entityEntry in entries)
            {
                // If the entity state is Added let's set
                // the CreatedAt and CreatedBy properties
                if (entityEntry.State == EntityState.Added)
                {
                    ((AuditableEntity)entityEntry.Entity).CreatedAt = Agent.Now;
                    ((AuditableEntity)entityEntry.Entity).CreatedBy = sessionContext?.UserId;
                }
                else
                {
                    // If the state is Modified then we don't want
                    // to modify the CreatedAt and CreatedBy properties
                    // so we set their state as IsModified to false
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedAt).IsModified = false;
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedBy).IsModified = false;
                }

                // In any case we always want to set the properties
                // ModifiedAt and ModifiedBy
                ((AuditableEntity)entityEntry.Entity).UpdatedAt = Agent.Now;
                ((AuditableEntity)entityEntry.Entity).UpdatedBy = sessionContext?.UserId;
            }

            // After we set all the needed properties
            // we call the base implementation of SaveChangesAsync
            // to actually save our entities in the database
            return await base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            // Get all the entities that inherit from AuditableEntity
            // and have a state of Added or Modified
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is AuditableEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            // For each entity we will set the Audit properties
            foreach (var entityEntry in entries)
            {
                // If the entity state is Added let's set
                // the CreatedAt and CreatedBy properties
                if (entityEntry.State == EntityState.Added)
                {
                    ((AuditableEntity)entityEntry.Entity).CreatedAt = Agent.Now;
                    ((AuditableEntity)entityEntry.Entity).CreatedAtDate = DateTime.Now;
                    ((AuditableEntity)entityEntry.Entity).CreatedBy = sessionContext?.UserId;
                }
                else
                {
                    // If the state is Modified then we don't want
                    // to modify the CreatedAt and CreatedBy properties
                    // so we set their state as IsModified to false
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedAt).IsModified = false;
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedAtDate).IsModified = false;
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedBy).IsModified = false;
                }

                // In any case we always want to set the properties
                // ModifiedAt and ModifiedBy
                ((AuditableEntity)entityEntry.Entity).UpdatedAt = Agent.Now;
                ((AuditableEntity)entityEntry.Entity).UpdatedAtDate = DateTime.Now;
                ((AuditableEntity)entityEntry.Entity).UpdatedBy = sessionContext?.UserId;
            }

            // After we set all the needed properties
            // we call the base implementation of SaveChangesAsync
            // to actually save our entities in the database
            return base.SaveChanges();
        }


    }
}
