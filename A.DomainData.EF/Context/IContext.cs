﻿

using DomainData.Interfaces;
using DomainCore.AppTransactions;
using DomainCore.AppUsers;
using DomainCore.General;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainCore.Entities.General;

using DomainCore.Entities.UserActions;
using DomainCore.Entities;
using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;

namespace DomainData.Ef.Context
{
    public interface IContext 
    {

        DbSet<User> Users { get; set; }
        DbSet<UserSetting> UserSettings { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<ActiveCode> ActiveCodes { get; set; }
        DbSet<Social> Socials { get; set; }
        DbSet<AboutUs> AboutUs { get; set; }
        DbSet<ContactUs> ContactUs { get; set; }
        DbSet<FaQ> FaQs { get; set; }
        DbSet<FaQCategory> FaQCategorys { get; set; }
        DbSet<Device> Devices { get; set; }
        DbSet<Document> Documents { get; set; }
        DbSet<PrivateDocument> PrivateDocuments { get; set; }
        DbSet<DocumentUser> DocumentUsers { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Province> Provinces { get; set; }
        DbSet<Update> Updates { get; set; }

        DbSet<Transaction> Transactions { get; set; }
        DbSet<TransactionCategory> TransactionCategories { get; set; }

        DbSet<UserNotification> Notifications { get; set; }
        DbSet<Slider> Sliders { get; set; }    
        DbSet<Comment> Comments { get; set; }
        DbSet<UserFavorite> UserFavorites { get; set; }
       
        DbSet<Chat> Chats { get; set; }
        DbSet<Message> Messages { get; set; }

        void SaveChanges();
        Task SaveChangesAsync();
    }
}
