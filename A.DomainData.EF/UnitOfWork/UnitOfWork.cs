﻿
using DomainData.Interfaces;
using DomainData.Ef.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Ef.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        public UnitOfWork(

            IUserRepository Users,
            IUserSettingRepository UserSetting,
            IUserRoleRepository UserRole,
            IRoleRepository Role,
            IActiveCodeRepository ActiveCode,
            IDeviceRepository Device,
            ISocialRepository Social,
            IFaQRepository FaQ,
            IFaQCategoryRepository FaQCategory,
            IAboutUsRepository AboutUs,
            IContactUsRepository ContactUs,
            IDocumentRepository Document,
            ICityRepository City,
            IProvinceRepository Province,
            ICountryRepository Country,
            ITransactionRepository Transaction,
            ITransactionCategoryRepository TransactionCategory,
            IUpdateRepository Update,
       
            ICommentRepository Comment,
            IUserFavoriteRepository UserFavorite,
            ISliderRepository Slider,
           
            IChatRepository Chat,
            IMessageRepository Message,


        IContext ctx
            )
        {
            this.Users = Users;
            this.UserSetting = UserSetting;
            this.UserRole = UserRole;
            this.Role = Role;
            this.ActiveCode = ActiveCode;
            this.Device = Device;
            this.Social = Social;
            this.FaQ = FaQ;
            this.FaQCategory = FaQCategory;
            this.AboutUs = AboutUs;
            this.ContactUs = ContactUs;
            this.Document = Document;
            this.City = City;
            this.Province = Province;
            this.Country = Country;
            this.Transaction = Transaction;
            this.TransactionCategory = TransactionCategory;
            this.Update = Update;
           
            this.Comment = Comment;
            this.UserFavorite = UserFavorite;
            this.Slider = Slider;
            
            this.Chat = Chat;
            this.Message = Message;
            this.ctx = ctx;

        }
        public IUserRepository Users { get; set; }
        public IUserSettingRepository UserSetting { get; set; }

        public IUserRoleRepository UserRole { get; set; }
        public IRoleRepository Role { get; set; }
        public IActiveCodeRepository ActiveCode { get; set; }
        public ISocialRepository Social { get; set; }
        public IFaQRepository FaQ { get; set; }
        public IFaQCategoryRepository FaQCategory { get; set; }
        public IAboutUsRepository AboutUs { get; set; }
        public IContactUsRepository ContactUs { get; set; }

        public IDeviceRepository Device { get; set; }
        public IDocumentRepository Document { get; set; }
        public ICityRepository City { get; set; }
        public IProvinceRepository Province { get; set; }
        public ICountryRepository Country { get; set; }
        public ITransactionRepository Transaction { get; set; }
        public ITransactionCategoryRepository TransactionCategory { get; set; }
        public IUpdateRepository Update { get; set; }
        
        public ICommentRepository Comment { get; set; }
        public IUserFavoriteRepository UserFavorite { get; set; }
        public ISliderRepository Slider { get; set; }
      
        public IChatRepository Chat { get; set; }
        public IMessageRepository Message { get; set; }
        public IContext ctx { get; set; }


        public void Complete()
        {
            ctx.SaveChanges();
        }
        public async Task CompleteAsync()
        {
            await ctx.SaveChangesAsync();
        }
    }
}
