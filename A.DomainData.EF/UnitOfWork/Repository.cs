﻿using DomainData.Interfaces;
using DomainData.Ef.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Ef
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        public   IQueryable<TEntity> baseInclude;
        private readonly DbContext Context;

        public Repository(IContext context  )
        {
            Context = (context as DbContext);

            baseInclude = Context.Set<TEntity>().AsQueryable();
        }
        public async Task<List<TEntity>> GetAllWithDetailAsync()
        {
            return await baseInclude.ToListAsync();

        }
        public async Task<TEntity> GetWithDetailAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await baseInclude.FirstOrDefaultAsync(predicate);

        }
        //public Task<TEntity> GetWithDetailAsync(object id)
        //{
        //    return  baseInclude.Find(id);
        //}

        public TEntity Get(object id)
        {
            return Context.Set<TEntity>().Find(id);
        }
        public async Task<TEntity> GetAsync(object id)
        {
           return  await Context.Set<TEntity>().FindAsync(id);
        }


        public List<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }


        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate);
        }
  

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().SingleOrDefault(predicate);
        }
          public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Context.Set<TEntity>().SingleOrDefaultAsync(predicate);
           // return await baseInclue.SingleOrDefaultAsync(predicate);
        }

        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        public void AddRange(List<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

        public async Task AddAsync(TEntity entity)
        {
           await Context.Set<TEntity>().AddAsync(entity);
        }

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            await Context.Set<TEntity>().AddRangeAsync(entities);
        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        } 
 

        public IEnumerable<TEntity> ExecuteStored(string ProcedureName, object[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
                ProcedureName += " @p" + i.ToString() + ",";
            ProcedureName = ProcedureName.Remove(ProcedureName.Length - 1, 1);
            return Context.Set<TEntity>().FromSqlRaw(ProcedureName, parameters);
        }





        public void Update(TEntity obj)
        {
            Context.Set<TEntity>().Update(obj);
        }

        public void Delete(object id)
        {
            var obj = Get(id);
            Context.Set<TEntity>().Remove(obj);
        }



        List<TEntity> IRepository<TEntity>.Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).ToList();
        }

        public async  Task<List<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await  Context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public T GetMax<T>(Expression<Func<TEntity, int>> expression)
        {
            if (Context.Set<TEntity>().Count() == 0)
                return (T)Convert.ChangeType(0, typeof(T));
            return (T)Convert.ChangeType(Context.Set<TEntity>().Max(expression), typeof(T));
        }

        public int GetCount()
        {
            return Context.Set<TEntity>().Count();
        }

        public int CountWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).Count();
        }
        public List<TEntity> GetWhereByRange(Expression<Func<TEntity, bool>> predicate , int start , int items)
        {
            return Context.Set<TEntity>().Where(predicate).Skip(start).Take(items).ToList();
        }

       
    }
}
