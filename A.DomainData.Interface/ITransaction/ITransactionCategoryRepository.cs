﻿
using DomainCore.AppTransactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Interfaces
{
    public interface ITransactionCategoryRepository : IRepository<TransactionCategory>
    {
    }
}
