﻿
using System;
using System.Collections.Generic;
using System.Text;
using DomainCore.AppTransactions;

namespace DomainData.Interfaces
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        int GetBalance(Guid userId);
        int GetTransactionsByPageCount(Guid userId);
        bool IsAuthorityExist(string authority);
        Transaction GetByAuthority(string authority);
        List<Transaction> GetLastYear(long lastYear);
    }
}
