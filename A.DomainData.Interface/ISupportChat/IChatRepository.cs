﻿using DomainCore.DTO.SupportChat;
using DomainCore.Entities.AppMore;
using DomainCore.Entities.SupportChat;
using System.Linq;

namespace DomainData.Interfaces
{
    public interface IChatRepository : IRepository<Chat>
    {
        public IQueryable<Chat> GetByFilter(GetChatByFilterDto dto);
    }

}
