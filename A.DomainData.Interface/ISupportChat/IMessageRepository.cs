﻿using DomainCore.Entities.SupportChat;

namespace DomainData.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
       
    } 

}
