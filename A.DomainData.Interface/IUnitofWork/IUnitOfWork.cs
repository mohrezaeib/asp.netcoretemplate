﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; set; }
        IUserSettingRepository UserSetting { get; set; }
        IUserRoleRepository UserRole { get; set; }
        IRoleRepository Role { get; set; }
        IActiveCodeRepository ActiveCode { get; set; }
        IDeviceRepository Device { get; set; }
        ISocialRepository Social { get; set; }
        IFaQRepository FaQ { get; set; }  
        IFaQCategoryRepository FaQCategory { get; set; }
        IAboutUsRepository AboutUs { get; set; }
        IContactUsRepository ContactUs { get; set; }
        IDocumentRepository Document { get; set; }
        ICityRepository City { get; set; }
        IProvinceRepository Province { get; set; }
        ICountryRepository Country { get; set; }
        ITransactionRepository Transaction { get; set; }
        ITransactionCategoryRepository TransactionCategory { get; set; }
        IUpdateRepository Update { get; set; }
   
        ICommentRepository Comment { get; set; }
        IUserFavoriteRepository UserFavorite { get; set; }
        ISliderRepository Slider { get; set; }
        
        IChatRepository Chat { get; set; }
        IMessageRepository Message { get; set; }


        void Complete();
        Task CompleteAsync();


    }
}
