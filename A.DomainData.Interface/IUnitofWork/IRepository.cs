﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<List<TEntity>> GetAllWithDetailAsync();
        Task<TEntity> GetWithDetailAsync(Expression<Func<TEntity, bool>> predicate);

        TEntity Get(object id);
        Task<TEntity> GetAsync(object id);
        int GetCount();
        List<TEntity> GetAll();
        Task<List<TEntity>> GetAllAsync();
        void Update(TEntity obj);
        void Delete(object id);
        List<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        void AddRange(List<TEntity> entities);
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(List<TEntity> entities);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> ExecuteStored(string ProcedureName, object[] parameters);
        T GetMax<T>(Expression<Func<TEntity, int>> expression);

        int CountWhere(Expression<Func<TEntity, bool>> predicate);
        List<TEntity> GetWhereByRange(Expression<Func<TEntity,
            bool>> predicate, int start, int items);


    }

}
