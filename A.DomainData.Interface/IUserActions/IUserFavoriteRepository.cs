﻿using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using DomainCore.Entities.UserActions;
using System.Linq;
using DomainCore.DTO.UserActions;
using DomainCore.DTO.Base;

namespace DomainData.Interfaces
{
    public interface IUserFavoriteRepository : IRepository<UserFavorite>
    {
        Task<List<UserFavorite>> GetByUserId(BaseByGuidDto dto);
        Task<List<UserFavorite>> IsExistAsync(AddUserFavoriteDto dto);
    }
}

