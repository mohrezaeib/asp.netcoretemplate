﻿using DomainCore.Entities.AppMore;

namespace DomainData.Interfaces
{
    public interface IFaQCategoryRepository : IRepository<FaQCategory>
    {
       
    }
}
