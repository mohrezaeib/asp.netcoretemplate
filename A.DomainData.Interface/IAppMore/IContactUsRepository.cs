﻿using DomainCore.DTO.AppMore;
using DomainCore.Entities.AppMore;
using System.Linq;

namespace DomainData.Interfaces
{
    public interface IContactUsRepository : IRepository<ContactUs>
    {
        IQueryable<ContactUs> GetByFilter(GetContactUsByFilterDto dto);
    } 
}
