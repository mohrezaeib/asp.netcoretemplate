﻿using DomainCore.Entities.AppMore;

namespace DomainData.Interfaces
{
    public interface ISocialRepository : IRepository<Social>
    {
      
    }
}
