﻿using DomainCore.Dto.General;
using DomainCore.Entities.AppMore;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface IAboutUsRepository : IRepository<AboutUs>
    {
       
    }  
}
