﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.AppUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        bool IsExist(string mobile);
        Task<User> GetByMobileAsync(string mobile);
        Task<User> GetByEmailAsync(string mobile);
        Task<User> GetDetailAsync(Guid Id);
        IQueryable<User> GetByFilter(GetUsersByFilterDto dto);

        //  ByPageResultDto<User> GetByFilter(GetUsersByFilterDto dto);



    }
}
