﻿using DomainCore.Entities;

namespace DomainData.Interfaces
{
    public interface IUserSettingRepository : IRepository<UserSetting>
    {
    }
}
