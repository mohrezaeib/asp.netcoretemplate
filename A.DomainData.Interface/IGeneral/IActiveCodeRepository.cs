﻿
using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Interfaces
{
    public interface IActiveCodeRepository : IRepository<ActiveCode>
    {
         
        bool CheckExeed(string mobile);
        bool CheckActiveCode(CheckActiveCodeDto dto);
    }

}
