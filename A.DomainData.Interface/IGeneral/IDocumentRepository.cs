﻿using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Interfaces
{
    public interface IDocumentRepository : IRepository<Document>
    {
        string GetUrl(Guid documentId);
    }
}
