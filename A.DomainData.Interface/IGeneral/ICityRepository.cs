﻿
using DomainCore.Dto.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface ICityRepository : IRepository<City>
    {
         Task<List<City>> GetByProvinceId(int provinceId);
   

    }
}
