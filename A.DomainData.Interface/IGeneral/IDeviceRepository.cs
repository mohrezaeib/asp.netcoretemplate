﻿
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainData.Interfaces
{
    public interface IDeviceRepository :  IRepository<Device>
    {
        bool IsExist(Guid userId, string deviceId);
         List<Device> GetAllUserDevice(Guid userId);
         Task<List<Device>> GetAllUserDeviceAsync(Guid userId);

    }
}
