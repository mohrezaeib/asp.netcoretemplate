﻿using DomainCore.Entities.General;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainData.Interfaces
{
    public interface ISliderRepository : IRepository<Slider>
    {
    }
}
