﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace Utility.Tools.SignalR.AuthForSignalR
{

    public static class JwtSignalRMiddleware
    {
        private static readonly String AUTH_QUERY_STRING_KEY = "access_token";

        public static void UseJwtSignalRMiddleware(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                if (string.IsNullOrWhiteSpace(context.Request.Headers["Authorization"]))
                {
                    try
                    {
                        if (context.Request.QueryString.HasValue)
                        {
                            Console.WriteLine("Token" );
                            var path = context.Request.Path;
                            if (path.StartsWithSegments("/chat")) { 
                            var token = context.Request.QueryString.Value
                                .Split('&').SingleOrDefault(x => x.Contains(AUTH_QUERY_STRING_KEY)).Split('=')[1];


                            if (!string.IsNullOrWhiteSpace(token) && path.StartsWithSegments("/chat"))
                            {
                               //context.Request.Headers.Add("Authorization", new[] { $"Bearer {token}" });
                               // await next.Invoke();

                               // Console.WriteLine( context.Request.Headers.ToString());
                            }}

                        }

                    }
                    catch
                    {
                        // if multiple headers it may throw an error.  Ignore both.
                    }
                }
                await next.Invoke();
            });

        }
    }





}
