﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Utility.Tools.SignalR.AuthForSignalR
{
    public static class Extention
    {



        public static void ConfigureJwtAuthForSignalR(this IApplicationBuilder app)
        {
            

            // add middleware to translate the query string token 
            // passed by SignalR into an Authorization Bearer header
            app.UseJwtSignalRMiddleware();

            // then add the standard JWT processing layer AFTER
            // the SignalR layer.
           // app.UseAuthentication();
         





        }

    }
}
