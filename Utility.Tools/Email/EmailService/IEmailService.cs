﻿using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System;

using Utility.Tools;

namespace Utility.Tools.EmailServies
{
    public interface IEmailService 
    {
        


        Task<string> SendAsync(string Text, string Destination);
        Task<string> SendAsync(string email, string ReceiverName, string subject, string body, bool isHtml = false);
    }
}
