﻿using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System;

using Utility.Tools;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace Utility.Tools.EmailServies
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration configuration;

        public EmailService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<string> SendAsync(string Text, string Destination)
        {
            configuration.GetSection<EmailParameters>();
            var client = new System.Net.Mail.SmtpClient(EmailParameters.Host, EmailParameters.Port.ToInt())
            {
                //UseDefaultCredentials = false,
                Credentials = new NetworkCredential(EmailParameters.Username, EmailParameters.Password),
                TargetName = EmailParameters.TargetName,
                EnableSsl = true
            };
            var mailMessage = new MailMessage
            {
                From = new MailAddress(EmailParameters.MailSender, EmailParameters.MailSenderDisplayName)
            };
            mailMessage.To.Add(Destination);
            mailMessage.Subject = EmailParameters.Subject;
            mailMessage.Body = Text;


            await client.SendMailAsync(mailMessage);


            return null;
        }

        public async Task<string> SendAsync(string email, string ReceiverName,  string subject, string body, bool isHtml = false)
        {
            configuration.GetSection<EmailParameters>();
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(EmailParameters.Username, EmailParameters.MailSenderDisplayName));
            message.To.Add(new MailboxAddress(ReceiverName, email));
            message.Subject = subject;

            var textFormat = isHtml ? TextFormat.Html : TextFormat.Plain;
            message.Body = new TextPart(textFormat)
            {
                Text = body
            };

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // Accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                await client.ConnectAsync(EmailParameters.Host, EmailParameters.PortInt, false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                 client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                await client.AuthenticateAsync(EmailParameters.Username, EmailParameters.Password);

                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
            return null;
        }


    }
}
