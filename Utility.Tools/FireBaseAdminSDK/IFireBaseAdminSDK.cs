﻿using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utility.Tools
{
    public interface IFireBaseAdminSDK
    {
        public  Task SendNotification(string token, Notification not);
        public  Task SendNotification(List<string> token, Notification not);
    }
}
