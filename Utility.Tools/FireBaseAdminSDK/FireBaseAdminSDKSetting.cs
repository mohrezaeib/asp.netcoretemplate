﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utility.Tools
{
    public class FireBaseAdminSDKSetting
    {
        /*
         *  "type": "service_account",
    "project_id": "ir-itamin-koohposh",
    "private_key_id": "ee4ea5aa68cc18dc44c8f26091ab310cb213cadc",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCQ/hO7M2EZDfNK\n+ImVVgMMowAfMPsUa0VdfhRFllamSU0d+yARycfMXUYdESu2/dsjWXSDXtevIXG2\nQP2jYTLfWXnarrI0C7hdqI8UZwEzYcL0UuhoFl3gR4H8De3qG4AY649x/DzikJal\n2ocZHNinUIRk63bWmmVY4b0ad9qDYf2qSPn9rYjGZGjYLmQ3C8MJ9RFaCO1/cpK8\nLeLhR+jyg7/7hZfeHaZx1nSmphMILR09wiVzTzjCqE95LN3zlH8If3QE26XzvNeL\nA5mJAShF9O4M0iv9vGOemMW7YZZsGasHZl2IDYkK/SQjMwENRzReSntq0MWzRT5c\nzrnYRw0bAgMBAAECggEAAWjVThJGsd5bjJ5UHBI9FYzdTfBSDu9CBxG+mbSUVnZ+\nj1MnJMHs6GgwHwBq1Kys48mVVkeZ2ip9kM0avVFb4L73Ov1MXzDz+fkb0JHcLNP/\nXkesc09jjaNN6+J1lLtV6FtPeccYqNOWAbomBEQuOP/EroR5ecZmqms7XuJmZE7D\nHDmL4PqgdMGdbwfSKoiRLXN+PQILI29RhZrKcfOtxw2xII8JVAy/ThTVn8r4o2hG\nMjDf/JbV121YsGfVv2eGYCUqCfzanOoxzZgcHRkaBLguaPotrc/3xQkihXMogqNV\nfHA9/WqFt453EMyjvG8GEEHMuWiEH3cNiBja2GHl3QKBgQDJBLCSSM4/dJQ7/Se/\nKmDUifsPWHySPrclj5XefmHI+2XHcI5ZMc1lDNeA3RqULJHr8UqB0dRPCVfichZl\nVUK0auhaNL6qQiBAOHAb1bwzX4XKW99Nu/8i3fLWHnogPd+VRdfBJON1n4ACOHtm\nlwJ3nYg31tSDXr2ZIApKZs6l5wKBgQC4pnTh+GqA0VzbTs8IuCbEtAYv7wgZPg6p\nMmpFGj14CAvGslvdrdmn98Ph5TEnWZJueEcHItFAffhXG4fiPcXislkvxYxT01yS\n6DWAgoW5OKWTAyArxY7PRuUKBMh1BEyZZ1ZDpFWzRc4hcu6twpcm4a7If0aAx9y2\nVbnPbFmQrQKBgQDHKNlUJcQKRJN1F+1gTjlfelF+s7nAH1jPYwImTWrOeJ0vSwoN\nvvTzsBR3fRiPQ9VTQV4DXwQNRoGHPmeIAEA1hlIsxWM+RoOzfPc97LYo4/QnOHz2\nhFNUlTe6AroW5ZZ2S5tmpgiiCeQGzzUbwHPWVQKX6EXm3l/UrVTkwBTvcwKBgB3F\nwRzaWBnTKBy4VUt7ncyjlkfOS8j+J5ECbv4bYAPlkDiGc6eHq4uYuH3I3acRmzmy\nrQkqhQDYDt3Es6E4Apst1JGOUbUGZGvPgyjoYrVdu9y7aUmFfiRPswhtgwDTI6Ks\ndc1MRwEz5Suhg3iC4qpDQ1JSq0SCdUT1B5O5Tau1AoGAJdo9He3j2QpyvN3SCYL6\nRZaUEYLBkRrAVDTun3rzPVvNyvr5A5QszcIupGrI2LnP9UJJwScgIWIdzqqirzpY\nfMT5x2XGHUVOPIPXUJtOcGIMcZTsVFo0u+4HjhWz458F6/FvEP08X15Ga14slzaN\nXqx1MPE/sUJ+v6rkskInOkM=\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-jfde4@ir-itamin-koohposh.iam.gserviceaccount.com",
    "client_id": "111311994174949660335",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/me
         * */
        public string type { get; set; }
        public string project_id { get; set; }
        public string private_key { get; set; }
        public string client_email { get; set; }
        public string auth_uri { get; set; }
        public string token_uri { get; set; }
        public string auth_provider_x509_cert_url { get; set; }
        public string client_x509_cert_url { get; set; }
    }
}
