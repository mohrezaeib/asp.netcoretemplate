﻿
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utility.Tools
{
    public class FireBaseAdminSDK : IFireBaseAdminSDK
    {
        private readonly FirebaseMessaging messaging;
       

        public FireBaseAdminSDK(IConfiguration configuration)
        {
            Configuration = configuration;
            var set = configuration.GetSection("FireBaseAdminSDKSettings").Get<FireBaseAdminSDKSetting>();
            
                var app = FirebaseApp.Create(new AppOptions() 
            //{ Credential = GoogleCredential.FromFile("serviceAccountKey.json")
           { Credential = GoogleCredential.FromJson(set.ToJson())
            .CreateScoped("https://www.googleapis.com/auth/firebase.messaging")
            });
            messaging = FirebaseMessaging.GetMessaging(app);
            
        }
        //...       

        public IConfiguration Configuration { get; }


        private Message CreateNotification(string token, Notification not)
        {
            return new Message()
            {
                Token = token,
                Notification = not
            };
        }

        public async Task SendNotification(string token, Notification not)
        {
            try
            {
                var str = await messaging.SendAsync(CreateNotification(token, not));
                Console.WriteLine(str);
                //do something with result
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public async Task SendNotification(List<string> tokens,Notification not)
        {
            // tokens.ForEach(async t => {

            //    await messaging.SendAsync(CreateNotification(title, body, t));
            //});
            foreach(var  t in tokens)
            {
                try
                {
                    var str= await messaging.SendAsync(CreateNotification( t, not));
                    Console.WriteLine(str);
                    //do something with result
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }
    }
}
