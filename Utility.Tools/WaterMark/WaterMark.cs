﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace Utility.Tools.AddWaterMark
{
    public static class WaterMark
    {

        public static MemoryStream AddTextWaterMark(this MemoryStream ms, string watermarkText, MemoryStream outputStream)
        {
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            Graphics gr = Graphics.FromImage(img);
            Font font = new Font("Tahoma", (float)40);
            Color color = Color.FromArgb(50, 241, 235, 105);
            double tangent = (double)img.Height / (double)img.Width;
            double angle = Math.Atan(tangent) * (180 / Math.PI);
            double halfHypotenuse = Math.Sqrt((img.Height * img.Height) + (img.Width * img.Width)) / 2;
            double sin, cos, opp1, adj1, opp2, adj2;

            for (int i = 100; i > 0; i--)
            {
                font = new Font("Tahoma", i, FontStyle.Bold);
                SizeF sizef = gr.MeasureString(watermarkText, font, int.MaxValue);

                sin = Math.Sin(angle * (Math.PI / 180));
                cos = Math.Cos(angle * (Math.PI / 180));
                opp1 = sin * sizef.Width;
                adj1 = cos * sizef.Height;
                opp2 = sin * sizef.Height;
                adj2 = cos * sizef.Width;

                if (opp1 + adj1 < img.Height && opp2 + adj2 < img.Width)
                    break;
                //
            }

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            gr.SmoothingMode = SmoothingMode.AntiAlias;
            gr.RotateTransform((float)angle);
            gr.DrawString(watermarkText, font, new SolidBrush(color), new Point((int)halfHypotenuse, 0), stringFormat);
            img.Save(outputStream, ImageFormat.Jpeg);
            return outputStream;

        }
        public static MemoryStream AddImageWaterMark(this MemoryStream ms , MemoryStream outputStream)
        {
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            
            Graphics gr = Graphics.FromImage(img);
            Image newImage = Image.FromFile("./WaterMark.png");
            double ratio =(double) newImage.Height/ (double)newImage.Width;
            int W = img.Width; 
            int H = img.Height; 
            int width = (int)(.1 * W);
            int height = (int) (width * ratio) ;

            // Create parallelogram for drawing image.
            Point ulCorner = new Point((W  - width -10), (H - height -10  ));
            Point urCorner = new Point((W - 10), ( H - height - 10));
            Point llCorner = new Point((W -width -10), (H - 10));
            Point[] destPara = { ulCorner, urCorner, llCorner };

            // Draw image to screen.
            gr.DrawImage(newImage, destPara);

            img.Save(outputStream, ImageFormat.Jpeg);
            return outputStream;

        }

    }

}

