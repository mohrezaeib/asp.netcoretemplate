﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Tools.Auth
{
    public static class Extensions
    {
        public static void AddJwt(this IServiceCollection services,IConfiguration config)
        {
            var Options = new JwtOptions();
            var section = config.GetSection("jwt");
            section.Bind(Options);
            services.Configure<JwtOptions>(section);
            services.AddScoped<IJwtHandler, JwtHandler>();
           
            services.AddAuthentication()
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false,
                        ValidIssuer = Options.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Options.SecretKey))
                    };
                    options.Events = new JwtBearerEvents
                    {
                        // only for SignalR
                        // because it has token in query 
                        // but we want it on header
                        // so we get token from query and put it in the header
                        
                        OnMessageReceived = context =>
                        {
                           
                            var path = context.Request.Path;
                            var accessToken = context.Request.Query["access_token"];
                            
                            if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/chat"))
                            {
                                //context.Token = accessToken;//what is it ?
                                context.Request.Headers["Authorization"] = new[] { $"Bearer {accessToken}" };
                                //context.Request.Headers.Add("Authorization", new[] { $"Bearer {accessToken}" });
                                
                            }
                            return Task.CompletedTask;
                        }
                    };
                });
        }
      




    }




}
