﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using Utility.Tools.Auth;
using LinqToDB;

namespace WebApi.Middleware
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration config;
        private readonly JwtOptions Options;

        public JwtMiddleware(RequestDelegate next, IConfiguration config)
        {
            _next = next;
            this.config = config;
             Options = new JwtOptions();
            var section = config.GetSection("jwt");
            section.Bind(Options);
        }

        public async Task Invoke(HttpContext context)
        {

            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                 attachAccountToContext(context, token);

            await _next(context);
        }

        private  void attachAccountToContext(HttpContext context,  string token )
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Options.SecretKey);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var accountId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "sub").Value);

                // attach account to context on successful jwt validation
                context.Items["UserId"] = accountId;
            }
            catch 
            {
                // do nothing if jwt validation fails
                // account is not attached to context so request won't have access to secure routes
            }
            //return Task.CompletedTask;
        }
    }
}
