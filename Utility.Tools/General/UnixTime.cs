﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility.Tools.General
{
    public static class UnixTime
    {
        public static DateTime ToDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static long ToUnix(DateTime dateTime)
        {
            return (long)((TimeZoneInfo.ConvertTimeToUtc(dateTime) -
           new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds);
        }

        public static long GetSpan(DateTime start , DateTime end)
        {
            return (long)((TimeZoneInfo.ConvertTimeToUtc(end) -
           TimeZoneInfo.ConvertTimeToUtc(start)).TotalSeconds);
        }
        public static long  StartOfDay(this DateTime theDate)
        {
            return theDate.ToUnix();
        }

        public static long EndOfDay(this DateTime theDate)
        {
            return theDate.Date.AddDays(1).AddTicks(-1).ToUnix();
        } 
        //public static long StartOfPersianWeek(this DateTime theDate)
        //{
        //   var today = ((( int)  theDate.DayOfWeek)+1)%7;

            
        //}

        //public static long EndOfPersianWeek(this DateTime theDate)
        //{
        //    return theDate.Date.AddDays(1).AddTicks(-1);
        //}
    }

}
