﻿using System.Threading.Tasks;

namespace Utility.Tools
{
    public interface INotification
    {
        Task<string> SendAsync(string Text,string Destination);
    }
   
}
