﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Microsoft.Extensions.Configuration;

namespace Utility.Tools.SMS.TwilioSMS
{
    public class TwilioService : ITwilioService
    {
        private readonly ITwilioRestClient client;
        private readonly IConfiguration config;

        public TwilioService(ITwilioRestClient _client , IConfiguration config)
        {
            client = _client;
            this.config = config;
        }
        public async  Task<MessageResource> SendAsync(string Text, string Destination)
        {
            var n = new PhoneNumber(config["Twilio:Number"]);
            var message =  await  MessageResource.CreateAsync(
               to: new PhoneNumber(Destination),
               from: new PhoneNumber(config["Twilio:Number"]),
               body: Text,
               client: client); // pass in the custom client
            return message;
        }



    }
    public interface ITwilioService
    {
        Task<MessageResource> SendAsync(string Text, string Destination);
    }
}
