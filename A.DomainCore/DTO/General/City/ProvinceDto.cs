﻿
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.General
{
    public class ProvinceDto
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int Id { get;  set; }
        public int CountryId { get; set; }

        public List<CityDto> Cities { get; set; }
    }
}
