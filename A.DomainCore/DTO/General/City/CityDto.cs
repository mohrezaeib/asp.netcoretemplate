﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public class CityDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }

    }
}
