﻿using DomainCore.Enums;

namespace DomainCore.Dto.General
{
    public class GetSplashDto
    {
        public int? VesionCode { get; set; }
        public OS OS { get; set; }
        public string PkgName { get; set; }
    }
}
