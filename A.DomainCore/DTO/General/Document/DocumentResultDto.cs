﻿
using DomainCore.Enums;
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.General
{
    public class DocumentDto
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public DocumentType Type { get; set; }
    }

}

