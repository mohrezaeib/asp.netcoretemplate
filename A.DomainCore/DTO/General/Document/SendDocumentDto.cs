﻿using DomainCore.Enums;
using Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public  class SendDocumentDto
    {
        public IFormFile File { get; set; }
        public DocumentType Type { get; set; }
       // public bool Watermark { get; set; }
    }
}
