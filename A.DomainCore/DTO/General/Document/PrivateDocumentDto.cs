﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public class PrivateDocumentDto
    {
        public string Address { get; set; }
        public string Location { get; set; }
        public string Alt { get; set; }
    }
}
