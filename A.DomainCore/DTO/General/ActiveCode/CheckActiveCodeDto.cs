﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public class CheckActiveCodeDto
    {
        public string Code { get; set; }
        public string Mobile { get; set; }
        //public string Email { get; set; }
        public string PushId { get; set; }
    }
  
}
