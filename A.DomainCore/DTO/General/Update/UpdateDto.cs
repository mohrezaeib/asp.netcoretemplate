﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{
    public class UpdateDto
    {
        
        public int? NewVesionCode { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public OS OS { get; set; }
        public string PkgName { get; set; }
        public bool HasUpdate { get; set; }
        public bool IsForce { get; set; }
 
    }
}
