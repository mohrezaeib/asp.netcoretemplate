﻿using DomainCore.Enums;

namespace DomainCore.Dto.General
{
    public class AddUpdateDto
    {
        public int? NewVersionCode { get; set; }
        public int? FromVersionCode { get; set; }
        public int? ToVersionCode { get; set; }
        public OS OS { get; set; }
        public string PkgName { get; set; }
        public string Description { get; set; }
        public bool HasUpdate { get; set; }
        public bool IsForce { get; set; }

    }
}
