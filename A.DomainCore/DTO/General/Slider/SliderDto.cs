﻿using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.Slider
{
   public  class SliderDto:BaseDto<int>
    {
        public string ExternalLink { get; set; }
        public bool IsInside { get; set; }
        public int? InternalLinkId { get; set; }
        public DocumentDto Document { get; set; }

    }
    public  class AddSliderDto
    {
        public string ExternalLink { get; set; }
        public bool IsInside { get; set; }
        public int? InternalLinkId { get; set; }
        public Guid DocumentId { get; set; }
      
    } 
    public  class EditSliderDto : AddSliderDto
    {
        public int Id { get; set; }

    }
}
