﻿
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.General
{

    public class LoginDto
    {

        public string UserName { get; set; }
        public bool IsMobile { get; set; }
        public string Password { get; set; }
        public string PushId { get; set; }
    } 
   
    public class ForgotPasswordDto
    {
        public string UserName { get; set; }
        public bool IsMobile { get; set; }

    }
    public class RegisterByEmailDto
    {
        public string Email { get; set; }
        //public string Password { get; set; }

    }
    public class ConfirmEmailDto
    {
        public string Email { get; set; }
        public string Code { get; set; }
        public string PushId { get; set; }

    }
    public class ChangePasswordDto
    {
        public Guid UserId { get; set; }
        public bool IsMobile { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

    }

}
