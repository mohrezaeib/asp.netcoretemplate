﻿using DomainCore.DTO.Base;
using System.Collections.Generic;

namespace DomainCore.DTO.AppMore
{
    public class AboutUsDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
       
        public List<SocialDto> Socials { get; set; }
    }
}
