﻿using System;

namespace DomainCore.DTO.AppMore
{
    public class AddContactUsDto
    {
       
        public string Message { get; set; }
        public Guid UserId { get; set; }

    }
}
