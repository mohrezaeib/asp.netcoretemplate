﻿using DomainCore.Dto.General;
using DomainCore.DTO.Base;
using DomainCore.Enums;

namespace DomainCore.DTO.AppMore
{
    public class SocialDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public SocialType SocialType { get; set; }

        public DocumentDto Image { get; set; }
    }
}
