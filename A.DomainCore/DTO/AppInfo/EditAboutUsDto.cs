﻿namespace DomainCore.DTO.AppMore
{
    public class EditAboutUsDto : AddAboutUsDto
    {
        public int Id { get; set; }
    }
}
