﻿using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Enums;

namespace DomainCore.DTO.AppMore
{
    public class ContactUsDto : BaseDto<int>
    {
       
        public string Message { get; set; }
        public UserShortDto User { get; set; }

        public ContactUsStatus Status { get; set; }
    }
}
