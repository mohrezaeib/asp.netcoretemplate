﻿using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;

namespace DomainCore.DTO.AppMore
{
    public class GetContactUsByFilterDto : BaseGetByPageDto
    {
        public string Keyword { get; set; }
        public long? From { get; set; }
        public long? To { get; set; }
        public Guid?  UserId { get; set; }
        public ContactUsStatus?  Status { get; set; }

    }
}
