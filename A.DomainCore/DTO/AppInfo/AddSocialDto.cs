﻿using DomainCore.Enums;
using System;

namespace DomainCore.DTO.AppMore
{
    public class AddSocialDto 
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public SocialType SocialType { get; set; }

        public Guid ImageId { get; set; }
    }
}
