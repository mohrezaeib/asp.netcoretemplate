﻿using System.Collections.Generic;

namespace DomainCore.DTO.AppMore
{
    public class AddAboutUsDto 
    {
        public string Title { get; set; }
        public string Description { get; set; }
 
    }
}
