﻿using DomainCore.DTO.Base;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.AppMore
{
    public class FaQShortDto : BaseDto<int>
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int CategoryId { get; set; }

    }
    public class FaQDto : FaQShortDto
    {
       
        public FaQCategoryShortDto Category { get; set; }

    }

    public class FaQCategoryShortDto : BaseDto<int>
    {
        public string Title { get; set; }

    }
    public class FaQCategoryDto : FaQCategoryShortDto
    {
     
        public List<FaQShortDto> FaQs { get; set; }

    }
    public class AddFaQCategoryDto 
    {
        public string Title { get; set; }
       

    }
    public class EditFaQCategoryDto :  AddFaQCategoryDto 
    {
        public int Id { get; set; }
       

    }
}
