﻿using DomainCore.Enums;

namespace DomainCore.DTO.AppMore
{
    public class EditContactUsStatusDto
    {
        public int Id { get; set; }
        public ContactUsStatus Status { get; set; }

    }
}
