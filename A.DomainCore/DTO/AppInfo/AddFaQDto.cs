﻿namespace DomainCore.DTO.AppMore
{
    public class AddFaQDto 
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int CategoryId { get; set; }
    }
}
