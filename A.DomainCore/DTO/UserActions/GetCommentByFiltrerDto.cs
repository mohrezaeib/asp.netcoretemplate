﻿using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;

namespace DomainCore.DTO.UserActions
{
    public class GetCommentByFiltrerDto: BaseGetByPageDto
    {

        public string ContentKeyword { get; set; }
        public string UserKeyword { get; set; }
        public Status? Status { get; set; }
        public Guid? SenderId { get; set; }
        public bool? IsAnswered { get; set; }
        public bool? IsFromAdmin { get; set; }
      
        public long? From { get; set; }
        public long? To { get; set; }


    }
}
