﻿using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.DTO.UserActions
{
    public class CommenDtoForMobile : BaseDto<Guid>
    {
        public string Text { get; set; }
        public Status Status { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsFromAdmin { get; set; }
        public Guid SenderId { get; set; }
        public Guid? ParentId { get; set; }
        public  UserShortDto Sender { get; set; }
        public List<CommenDtoForMobile> Comments { get; set; }
    } 
    public class CommentBigDto : BaseDto<Guid>
    {
        public string Text { get; set; }
        public Status Status { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsFromAdmin { get; set; }

        public Guid SenderId { get; set; }
        public Guid? ParentId { get; set; }
        public  UserShortDto Sender { get; set; }
        public List<CommentBigDto> Comments { get; set; }
    

    }
    public class AddCommentDto 
    {
        [Required]
        public string Text { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? SenderId { get; set; }
     


    }
    public class AddCommentAnswerDto
    {

        public string Text { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? SenderId { get; set; }


    }
    public class EditCommentDto : AddCommentDto
    {

        public Guid Id { get; set; }
        public Status Status { get; set; }


    }
}
