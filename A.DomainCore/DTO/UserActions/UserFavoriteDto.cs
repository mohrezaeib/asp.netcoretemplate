﻿using DomainCore.Dto.User;
using DomainCore.DTO.Base;

using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.UserActions
{
    public class UserFavoriteDto : BaseDto<Guid>
    {
     
    }
    public class AddUserFavoriteDto 
    {
        public Guid UserId { get; set; }
        
    }

    public class UserFavoriteListDto : BaseDto<Guid>
    {
       
    }
}
