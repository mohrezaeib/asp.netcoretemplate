﻿using DomainCore.Enums;
using System;

namespace DomainCore.DTO.SupportChat
{
    public class EditMessageByAdminDto
    {
        public Guid Id { get; set; }
        public Guid? DocumentId { get; set; }
        public bool? IsAdmin { get; set; }
        public string Text { get; set; }
        public MessageStatus? MessageStatus { get; set; }

    }


}
