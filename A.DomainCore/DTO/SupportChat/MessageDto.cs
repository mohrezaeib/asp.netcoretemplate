﻿using DomainCore.Dto.General;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;

namespace DomainCore.DTO.SupportChat
{
    public class MessageDto : BaseDto<Guid>
    {

        public virtual UserShortDto Sender { get; set; }
        public virtual DocumentDto Document { get; set; }
        public bool IsAdmin { get; set; }
        public MessageStatus MessageStatus { get; set; }
        public Guid ChatId { get; set; }
        public Guid TempId { get; set; }

        public string Text { get; set; }

    }
}
