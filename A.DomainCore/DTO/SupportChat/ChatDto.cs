﻿using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.SupportChat
{
    public class ChatDto : BaseDto<Guid>
    {

        public UserShortDto Customer { get; set; }
        public List<MessageDto> Messages { get; set; }

    }

    public class StartSupportChatDto 
    {

        public ChatDto Chat { get; set; }
        public bool IsAnySupportOnline { get; set; }

    }
}
