﻿using DomainCore.DTO.Base;
using System;

namespace DomainCore.DTO.SupportChat
{
    public class SendMessageDto 
    {

        public string Text { get; set; }
        public Guid? DocumentId { get; set; }
        public Guid SenderId { get; set; }
        public bool IsAdmin { get; set; }
        public Guid ChatId { get; set; }
        public Guid TempId { get; set; }

    }

    public class GetChatByFilterDto : BaseGetByPageDto
    {

      
        public Guid? ChatId { get; set; }
        public bool? HasUnSeenMessage { get; set; }
        public DateTime? MessageDate { get; set; }
    }

    public class RegisterConnectionDto 
    {


        public bool IsAdmin { get; set; }
    }

}
