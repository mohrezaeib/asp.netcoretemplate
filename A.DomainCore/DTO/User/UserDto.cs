﻿

using DomainCore.Dto.General;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Utility.Tools.Auth;

namespace DomainCore.Dto.User
{
    public class RoleDto
    {
        public Roles RoleId { get; set; }
        public string Name { get; set; }
    }
    public class EditUserSettingDto
    {
        public bool SendEmail { get; set; }
        public bool SendSMS { get; set; }
        public bool SendNotification { get; set; }
       
    }
    public class UserSettingDto
    {
        public bool SendEmail { get; set; }
        public bool SendSMS { get; set; }
        public bool SendNotification { get; set; }
    }
    public class UserDto
    {
        public UserDto()
        {
            UserSetting = new UserSettingDto();
        }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get;  set; }
        [DataType(DataType.Date)]
        public DateTime? BirthdayDateTime { get;  set; }
        public string Birthday { get;  set; }
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        public string Bio { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public UserStatus Status{ get; set; }
        public CityDto City { get; set; }
        public List<RoleDto> Roles { get; set; }
        public JsonWebToken Token { get; set; }
        public long CreatedAt { get;  set; }
        public int? Balance { get;  set; }
        public Genders? Gender { get;  set; }
        public DocumentDto ProfileImage { get;  set; }
        public List<DocumentDto> Documents { get;  set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string GiftCode { get; set; }
        public string FileNo { get; set; }
        public bool? HasPassword { get; set; }
        public UserSettingDto UserSetting { get; set; }


    }

  
    public class SetPasswordByUserDto
    {
        public Guid UserId { get; set; }
        public string Password { get; set; }

    }
    public class EditProfileDto: AddUserDto
    {
        public Guid UserId { get; set; }

    }
    public class EditProfileByAdminDto : AddUserByAdminDto
    {
        public UserStatus Status { get; set; }
        public Guid UserId { get; set; }


    }
    public class AddUserByAdminDto : AddUserDto
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public List<AddUserRoleDto> Roles { get; set; }
        public string FileNo { get; set; }
        public List<Guid> Documents { get; set; }


    }
    public class AddUserRoleDto
    {
        public Roles RoleId { get; set; }
        public int? ClinicId { get; set; }
    }
    public class AddUserDto
    {
        public string Name { get; set; }
        public string FamilyName { get;  set; }
        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        public string Bio { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int? CityId { get; set; }
        public Genders? Gender { get;  set; }
        public Guid? ProfileImageId { get;  set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string GiftCode { get; set; }
    }
    public class UserResisterDto
    {
        public UserDto User { get; set; }
        public bool HasPassword { get; set; }
        public bool IsNew { get; set; }
    }
    public class UserShortDto
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public string Mobile { get;  set; }
        public string Email { get;  set; }
        public DocumentDto ProfileImage { get;  set; }
    }   
    public class EditUserStatustDto
    {
        public Guid UserId { get; set; }
        public int Status { get;  set; }
    }


    }
