﻿using DomainCore.DTO.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.User
{
    public  class GetUsersByFilterDto : BaseGetByPageDto
    {
        public int? CityId { get; set; }
        public UserStatus? Status { get; set; }
        public Genders? Gender { get; set; }
        public List<Roles> Role { get; set; }
        public int NameSorting { get; set; }
        public int FileNoSorting { get; set; }
        public string Keyword { get; set; }
        public string KeywordEmail { get; set; }
        public string KeywordName { get; set; }
        public string KeywordPhone { get; set; }
        public string KeywordFileNo { get; set; }

        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

    }
}
