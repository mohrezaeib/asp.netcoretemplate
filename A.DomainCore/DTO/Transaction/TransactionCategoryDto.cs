﻿
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.Transaction
{
    public class TransactionCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

   
}
