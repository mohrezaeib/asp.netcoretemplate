﻿
using DomainCore.Dto.Base;
using System;
using System.Collections.Generic;

namespace DomainCore.Dto.Base
{
    public class ApiResult : BaseApiResult
    {

        public object Data { get; set; }
    }

    public class ApiResult<T> : BaseApiResult
    {
        public T Data { get; set; }

    }

    public class ApiListResult<T> : BaseApiResult
    {
        public List<T> Data { get; set; }
    }
}


