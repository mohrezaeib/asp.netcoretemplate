﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Dto.Base
{
    public class PageDto
    {
        public PageDto()
        {
                
        }

        public int PageNo { get; set; }
        public int ItemPerPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
        
       
    }

    public class ApiPageResult : ApiResult
    {
        public ApiPageResult ()
        {
            Pages = new PageDto();
        }

        public PageDto Pages { get; set; }


    }
    public class ApiPageResult<T>: ApiListResult<T>
    {
        public ApiPageResult()
        {
            PageDto = new PageDto();
           
           
        }

        public PageDto PageDto { get; set; }


    }

 

}
