﻿namespace DomainCore.DTO.Base
{
    public class BaseByIntDto
    {
        public int Id { get; set; }
    }
}
