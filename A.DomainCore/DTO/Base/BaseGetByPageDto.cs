﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.Base
{
    public  class BaseGetByPageDto
    {
        public int? PageNo { get; set; } = 1;
        public int? ItemperPage { get; set; }
    }
}
