﻿using DomainCore.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utility.Tools.General;


namespace DomainCore.Dto.Base
{
    public class BaseApiResult
    {
        public BaseApiResult()
        {
           //Errors = new Exception[0];
        }

        public bool Status { get; set; }
        public string Message { get; set; }
        public string ClientMessage { get; set; }
        public ErrorCodes ErrorCode { get; set; }
        public string StackTrace { get; set; }
      
        public BaseApiResult Error(Exception e)
        {
            Status = false;
            Message = e.Message;
            // var str = Agent.ToJson(e);
            //str = Regex.Unescape(str);
            //string regexPattern = "\"([^\"]+)\":"; // the "propertyName": pattern
            //str = Regex.Replace(str, regexPattern, "$1:");

            //  StackTrace = str;
            ClientMessage = "Sorry, A Server Error Occured.";
            return this;
        } 
        public BaseApiResult Error( ErrorCodes errorCode , string e=null)
        {
            Status = false;
            ErrorCode = errorCode;
            Message =  Agent.GetEnumDescription(errorCode) ;
            ClientMessage = e;
            return this;
        }
    }
}
