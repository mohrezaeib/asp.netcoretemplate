﻿using DomainCore.DTO.Base;
using DomainCore.Enums;

namespace DomainCore.Dto.Base
{
    public class BaseByFilter : BaseGetByPageDto
    {
        public Status? Status { get; set; }
        public long? From { get; set; }
        public long? To { get; set; }
    }
}