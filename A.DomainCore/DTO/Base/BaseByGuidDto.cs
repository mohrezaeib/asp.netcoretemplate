﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.Base
{
    public class BaseByGuidDto
    {
        public Guid Id { get; set; }
    }
}
