﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.DTO.Base
{
    public  class BaseDto<T>
    {
        public T Id { get; set; }
        public long CreatedAt { get; set; }
        public DateTime CreatedAtDate { get; set; }
        public long? UpdatedAt { get; set; }
        public DateTime? UpdatedAtDate { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
    }
    public  class BaseDto : BaseDto<Guid>
    {
    }
}
