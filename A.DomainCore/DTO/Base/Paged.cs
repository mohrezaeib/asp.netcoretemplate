﻿using DomainCore.DTO.Base;
using DomainCore.General;
using LinqToDB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainCore.Dto.Base
{
    public interface IPaged<T>
    {
        ///<summary>
        /// Get the total entity count.
        ///</summary>
        int Count { get; }

        ///<summary>
        /// Get a range of persited entities.
        ///</summary>
        List<T> GetAll();
        Task<List<T>> GetAllAsync();
        Task<List<T>> GetRawItemsAsync();
        List<T> GetRawItmes();
        PageDto GetPageDto();


    }


    public class Paged<T> : IPaged<T>
    {
        private readonly IQueryable<T> source;
        private readonly BaseGetByPageDto filter;



        public Paged(IQueryable<T> source, BaseGetByPageDto filter)
        {
            this.source = source;
            filter.ItemperPage = (filter.ItemperPage.HasValue && filter.ItemperPage > 0) ? filter.ItemperPage : AdminSettings.Block;
            filter.PageNo = (filter.ItemperPage > 1 && filter.PageNo.HasValue)  ? filter.PageNo : 1;
            this.filter = filter;

        }

        public List<T> GetAll()
        {
            return source.ToList();
        }
        public async Task<List<T>> GetAllAsync()
        {
            return await source.ToAsyncEnumerable().ToList();
        }



        public int Count
        {
            get { return source.Count(); }
        }



        public async Task<List<T>> GetRawItemsAsync()
        {
            return await source.Skip((filter.PageNo.Value - 1) * filter.ItemperPage.Value).Take(filter.ItemperPage.Value).ToAsyncEnumerable().ToList();
        }

        public List<T> GetRawItmes()
        {
            return source.Skip((filter.PageNo.Value - 1) * filter.ItemperPage.Value).Take(filter.ItemperPage.Value).ToList();
        }

        public PageDto GetPageDto()
        {
            var page = new PageDto();

            page.ItemPerPage = filter.ItemperPage.Value;
            page.PageNo = filter.PageNo.Value;
            page.TotalItems = Count;
            page.TotalPages = (int)Math.Ceiling((decimal)((decimal)page.TotalItems / (decimal)filter.ItemperPage.Value));
            return page;

        }
    }

    public class Paged<T, S> : Paged<T>, IPaged<T>
    {

        private readonly IQueryable<T> source;
        private readonly BaseGetByPageDto filter;
        private readonly Func<T, S> dtoBuilder;



        public Paged(IQueryable<T> source, BaseGetByPageDto filter, Func<T, S> dtoBuilder) : base(source, filter)
        {
            this.source = source;
            this.filter = filter;
            this.dtoBuilder = dtoBuilder;
        }
        public List<S> GetItmes()
        {
            return source.Skip((filter.PageNo.Value - 1) * filter.ItemperPage.Value).Take(filter.ItemperPage.Value).Select(p=>dtoBuilder(p) ).ToList();
        } 
        public async Task<List<S>> GetItmesAsync()
        {
            var resp = await source.Skip((filter.PageNo.Value - 1) * filter.ItemperPage.Value).Take(filter.ItemperPage.Value).ToListAsync();
            return resp.Select(dtoBuilder).ToList();
        }
    }
}

