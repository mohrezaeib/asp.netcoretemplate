﻿using DomainCore.AppUsers;
using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Entities.SupportChat
{
    public class Chat : BaseEntity<Guid>
    {
        public Guid CustomerId { get; set; }
        public User Customer { get; set; }
        public List<Message> Messages { get; set; }
    }
}
