﻿using DomainCore.AppUsers;
using DomainCore.Base;
using DomainCore.Enums;
using DomainCore.General;
using System;

namespace DomainCore.Entities.SupportChat
{
    public class Message : BaseEntity<Guid>
    {
        public Guid SenderId { get; set; }
        public virtual User Sender { get; set; }
        public Guid? DocumentId { get; set; }
        public virtual Document Document { get; set; }
        public  string Text { get; set; }
        public bool IsAdmin { get; set; }
        public MessageStatus MessageStatus { get; set; }
        public Guid ChatId { get; set; }
        public Chat Chat { get; set; }
        public Guid TempId { get; set; }

    }
}
