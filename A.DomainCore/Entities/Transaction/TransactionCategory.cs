﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppTransactions
{
    public class TransactionCategory :BaseEntity<int>
    {
        public string Name{ get; set; }
        public virtual List<Transaction> Transaction { get; set; }

    }
}
