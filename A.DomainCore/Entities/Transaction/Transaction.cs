﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppTransactions

{
    public class Transaction :BaseGuidEntity
    {

        public int TransactionCategoryId { get; set; }
        public virtual TransactionCategory TransactionCategory { get; set; }
        public string Authority { get; set; }
        public string RefId { get; set; }
        public bool IsSuccessful { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }

        public Guid UserId { get; set; }
        public  virtual AppUsers.User User { get; set; }


        public string Json { get; set; }
        public int Type { get; set; }
        public int Target { get; set; }
    }
}
