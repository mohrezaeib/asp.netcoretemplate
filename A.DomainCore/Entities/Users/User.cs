﻿
using DomainCore.Base;
using Utility.Tools.Auth;
using DomainCore.General;
using System;
using System.Collections.Generic;
using DomainCore.Enums;
using System.ComponentModel.DataAnnotations;
using DomainCore.Entities.UserActions;
using DomainCore.Entities;
using DomainCore.Entities.SupportChat;

namespace DomainCore.AppUsers
{
    public class User : BaseGuidEntity
    {
        public User()
        {
           // UserSetting = new UserSetting();
        }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email{ get; set; }
        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }
        public Genders?  Gender{ get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string EmailConfirmationCode { get; set; }
        public UserStatus Status{ get; set; }

        public Guid? ReferenceId { get; set; }
        public User Reference { get; set; }
        public string Bio{ get; set; }
        public virtual City City { get; set; }
        public int? CityId { get; set; }
        public virtual  Document ProfileImage { get; set; }
        public Guid? ProfileImageId { get; set; }
        public virtual  UserSetting UserSetting { get; set; }
        public Guid? UserSettingId { get; set; }
        public string Salt { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string PostalCode { get; set; }
        public string GiftCode { get; set; }
        public string FileNo { get; set; }

        public  virtual List<UserFile> Documents{ get; set; }
        public  virtual List<Device> Device{ get; set; }
        public virtual  List<UserNotification> Notification { get; set; }
        public virtual  List<UserRole> UserRole { get; set; }
        public  virtual List<Comment> Comments { get; set; }
        public virtual List<UserFavorite> UserFavorite { get; set; }
  
        public virtual List<AppTransactions.Transaction> Transaction { get; set; }


        //support chat
        #region
        public virtual List<Message> Messages { get; set; }
        public virtual Chat SupportChat { get; set; }
        public Guid? SupportChatId { get; set; }
        #endregion

        public void SetPassword(string password, IEncrypter encrypter)
        {
            Salt = encrypter.GetSalt();
            Password = encrypter.GetHash(password, Salt);
        }

        public bool Search(string key)
        {
            return (key == null || ((Email != null && Email.Contains(key)) ||
                                    (Name != null && Name.Contains(key)) ||
                                    (FamilyName != null && FamilyName.Contains(key)) ||
                                    (Mobile != null && Mobile.Contains(key))
                    ));
        }

        public bool ValidatePassword(string password, IEncrypter encrypter) =>
            Password.Equals(encrypter.GetHash(password, Salt));
    }

}
