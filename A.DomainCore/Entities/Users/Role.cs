﻿using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppUsers
{
    public class Role
    {
        public Roles Id { get; set; }
        public string Name { get; set; }
        public virtual List<UserRole> UserRole { get; set; }

    }
}
