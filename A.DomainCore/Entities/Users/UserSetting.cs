﻿using DomainCore.AppUsers;
using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Entities
{
    public class UserSetting:BaseEntity<Guid>
    {
        public bool SendEmail { get; set; }
        public bool SendSMS { get; set; }
        public bool SendNotification { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
    }
}
