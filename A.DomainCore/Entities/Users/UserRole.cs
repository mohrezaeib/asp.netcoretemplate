﻿using DomainCore.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppUsers
{
    public class UserRole : BaseGuidEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public Roles RoleId { get; set; }
        public virtual  Role Role { get; set; }

    } 
}
