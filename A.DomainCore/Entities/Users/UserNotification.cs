﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.AppUsers
{
    public class UserNotification:BaseGuidEntity
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string DeviceId { get; set; }
        public string Image { get; set; }
        public Guid UserId{ get; set; }
        public virtual User User { get; set; }
    }
}
