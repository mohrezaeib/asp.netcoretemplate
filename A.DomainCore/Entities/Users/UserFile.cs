﻿using DomainCore.Base;
using DomainCore.General;
using System;

namespace DomainCore.AppUsers
{
    public class UserFile : BaseGuidEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public Guid DocumentId { get; set; }
        public  virtual Document Document { get; set; }
    }
}
