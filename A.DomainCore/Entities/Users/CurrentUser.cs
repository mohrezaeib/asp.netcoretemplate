﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Utility.Tools.Auth;

namespace DomainCore.Entities
{
    public class CurrentUser : ICurrentUser
    {
        private readonly JwtOptions Options;
        private readonly IConfiguration configuration;

        public CurrentUser(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {

            


            var userId = new object();
            if (httpContextAccessor != null && httpContextAccessor.HttpContext != null)
            {
                httpContextAccessor.HttpContext.Items.TryGetValue("UserId", out userId);
                if (userId != null) UserId = Guid.Parse(userId.ToString());
                else
                {
                    this.configuration = configuration;

                    Options = new JwtOptions();
                    var section = configuration.GetSection("jwt");
                    section.Bind(Options);
                    var token = httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

                    if (token != null)
                        attachAccountToContext(httpContextAccessor.HttpContext, token);


                }


            }

        }

        public Guid? UserId { get; set; }

        private void attachAccountToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Options.SecretKey);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var accountId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "sub").Value);

                // attach account to context on successful jwt validation
                UserId = accountId;
            }
            catch
            {
                // do nothing if jwt validation fails
                // account is not attached to context so request won't have access to secure routes
            }
            //return Task.CompletedTask;
        }
    }
    public interface ICurrentUser
    {
        public Guid? UserId { get; set; }
    }
}
