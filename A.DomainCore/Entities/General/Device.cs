﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainCore.General
{
    public class Device: BaseGuidEntity
    {
        public Guid UserId { get; set; }
        public virtual  AppUsers.User User { get; set; }
        public string PushId{ get; set; }

    }


}


