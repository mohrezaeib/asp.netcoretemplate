﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCore.Base;

namespace DomainCore.General
{
    public class ActiveCode : BaseGuidEntity
    {
        public string Code { get; set; }
        public string Mobile { get; set; }

    }
}
