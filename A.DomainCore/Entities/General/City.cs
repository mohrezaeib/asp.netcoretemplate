﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainCore.General
{
    public class City :BaseEntity<int>
    {
        public string Name { get; set; }
        public string PostalCode { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public int ProvinceId { get; set; }
        public virtual Province Province { get; set; }
       public virtual List<AppUsers.User> User { get; set; }
    }
}


