﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.General
{
    public class PrivateDocument:Document
    {
        public string FileName { get; set; }
        public virtual List<DocumentUser> Users { get; set; }
    }
}
