﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.General
{
    public class DocumentUser :BaseGuidEntity
    {
        public virtual PrivateDocument Document { get; set; }
        public  Guid DocumentId { get; set; }
        public  virtual  AppUsers.User User { get; set; }
        public  Guid UsertId { get; set; }

    }
}
