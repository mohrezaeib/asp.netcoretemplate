﻿using DomainCore.Base;
using DomainCore.Enums;
using DomainCore.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.Entities.General
{
    public class Slider: BaseEntity<int>
    {
        public string ExternalLink { get; set; }
        public bool IsInside { get; set; }
        public int? InternalLinkId { get; set; }
        public virtual Document Document { get; set; }
        public Guid DocumentId { get; set; }
    }
}
