﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainCore.General
{
    public class Country :BaseEntity<int>
    {
        public string Name { get; set; }
        public virtual List<Province> Province{ get; set; }
    }
}


