﻿using DomainCore.AppUsers;
using DomainCore.Base;
using DomainCore.Entities.General;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainCore.General
{
    public class Document : BaseGuidEntity
    {
        public string Location{ get; set; }        
        public DocumentType DocumentType{ get; set; }
        public long Size{ get; set; }
        public int Duration{ get; set; }
        public string Name{ get; set; }
        public virtual  List<User> Users{ get; set; }
        public virtual  List<Slider> Sliders{ get; set; }
      
        
    }
}


