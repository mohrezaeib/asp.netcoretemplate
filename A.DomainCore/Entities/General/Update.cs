﻿using DomainCore.Base;
using DomainCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCore.General
{
    public class Update : BaseGuidEntity
    {
        public int? NewVersionCode { get; set; }
        public int? FromVersionCode { get; set; }
        public int? ToVersionCode { get; set; }
        public OS OS { get; set; }
        public string PkgName { get; set; }
        public bool IsForce { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
