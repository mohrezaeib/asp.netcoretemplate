﻿using DomainCore.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainCore.General
{
    public class Province :BaseEntity<int>
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public string ShortName{ get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
        public  virtual List<City> Cities { get; set; }

    }


}


