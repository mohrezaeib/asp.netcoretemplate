﻿using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace DomainCore.Base
{
    public abstract class BaseGuidEntity : BaseEntity<Guid>
    {
        
    }
    public abstract class BaseEntity<T> : AuditableEntity
    {
        public T Id { get; set; }

      

        public BaseEntity()
        {
            CreatedAt = Agent.Now;
            CreatedAtDate = DateTime.Now;
        }


    }

    public class AuditableEntity 
    {
        public bool isDeleted { get; set; } 
        public long? UpdatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public long CreatedAt { get; set; }
        public DateTime CreatedAtDate { get; set; }
        public DateTime? UpdatedAtDate { get; set; }



    }
}
