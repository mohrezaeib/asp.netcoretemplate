﻿using DomainCore.AppUsers;
using DomainCore.Base;

using System;
using System.Collections.Generic;
using System.Text;
using DomainCore.Enums;

namespace DomainCore.Entities.UserActions
{
    public class Comment:BaseEntity<Guid>
    {
        public string Text { get; set; }
        public Status Status { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsFromAdmin { get; set; }

        public Guid SenderId { get; set; }
        public virtual User Sender { get; set; }
        public Guid? ParentId { get; set; }
        public virtual Comment Parent { get; set; }
        public  virtual List<Comment> Comments { get; set; }

 
    }
}
