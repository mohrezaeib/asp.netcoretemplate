﻿using DomainCore.Base;
using System.Collections.Generic;

namespace DomainCore.Entities.AppMore
{
    public class AboutUs:BaseEntity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual List<Social> Socials { get; set; }
    }
}
