﻿using DomainCore.Base;
using System.Collections.Generic;

namespace DomainCore.Entities.AppMore
{
    public class FaQCategory : BaseEntity<int>
    {
        public string Title { get; set; }
        public virtual List<FaQ> FaQs { get; set; }
    }
}
