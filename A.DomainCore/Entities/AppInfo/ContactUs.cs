﻿using DomainCore.AppUsers;
using DomainCore.Base;
using DomainCore.Enums;
using System;

namespace DomainCore.Entities.AppMore
{
    public class ContactUs : BaseEntity<int>
    {
      
        public string Message { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public ContactUsStatus Status { get; set; }


    }
}
