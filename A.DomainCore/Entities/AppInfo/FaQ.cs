﻿using DomainCore.Base;

namespace DomainCore.Entities.AppMore
{
    public class FaQ : BaseEntity<int>
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int CategoryId { get; set; }
        public virtual FaQCategory Category { get; set; }
    

    }
}
