﻿using DomainCore.Base;
using DomainCore.Enums;
using DomainCore.General;
using System;
using System.Text;

namespace DomainCore.Entities.AppMore
{


    public class Social : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public virtual Document Image { get; set; }
        public Guid? ImageId { get; set; }
        public int? AboutUsId { get; set; }
        public  virtual AboutUs AboutUs { get; set; }
        public SocialType SocialType { get; set; }
    }
}
