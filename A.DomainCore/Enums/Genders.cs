﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.Enums
{
    public enum Genders
    {
        [Display(Description = "other")]
        Other = 0,
        [Display(Description = "female")]
        Female = 1,
        [Display(Description = "male")]
        Male = 2
    }
}
