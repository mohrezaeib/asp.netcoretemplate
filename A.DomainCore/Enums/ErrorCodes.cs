﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainCore.Enums
{
 
    public enum ErrorCodes
    {
        [Display(Description = "User Not Found")]
        UserNotFound = 1,
        [Display(Description = "User Not Activated")]
        UserNotActivated = 2,
        [Display(Description = "User Not Allowed")]
        UserNotAllowed = 3,
        [Display(Description = "Wrong Password")]
        WrongPassword = 8, 
        [Display(Description = "Can Not  Found the Entity")]
        EntityNotFound = 9,
        [Display(Description = "Can Not Delete  the Entity, The entity has dependencies, please remove them first")]
        EntityCouldNotDeleted = 10,
        [Display(Description = "Package BodyParts should all come from one service")]
        PKGBodyShouldHaveOneService = 11,  
        [Display(Description = "Package BodyParts should have  one service")]
        PKGBodyPartError2 = 15,
        [Display(Description = "Parent Comment Not Exist")]
        ParentCommentNotExist = 12,
        [Display(Description = "There is already an user with this email")]
        UserExist = 13,
        [Display(Description = "Wrong Confirmation Code")]
        WrongConfirmationCode = 14,
        [Display(Description = "TimeIsTaken")]
        TimeIsTaken = 16, 



        [Display(Description = "You Have This Service")]
        YouHavethisService = 17,
         [Display(Description = "TheClinicAlreadeyHasThisservice")]
        TheClinicAlreadeyHasThisService = 18,
         [Display(Description = "TheClinicAlreadeyHasThisPackage")]
        TheClinicAlreadeyHasThisPackage = 19,
         
         [Display(Description = "First Add Related Service")]
        RelatedServiceCouldNotFound = 20,       
      
         [Display(Description = "Service Time is less than 15 minutes")]
        AppointmentTimeIsTooShort = 21,
     
         [Display(Description = "TimeSpanDoesNotMatchrequiredTime")]
        TimeSpanDoesNotMatchrequiredTime = 22,

        [Display(Description = "RelatedServiceCouldNotFoundIntheClinic")]
        RelatedServiceCouldNotFoundInTheClinic = 23,

        
        [Display(Description = "No body Part Found")]
        NobodyPartFound = 24,

        [Display(Description = "Failed To Reschedule")]
        FailedToReschedule = 25,
        [Display(Description = "You Cannot Reschedule This Appointment")]
        YouCannotRescheduleThisAppointment = 26,

        [Display(Description = "CouldNotFindGiftCode")]
        CouldNotFindGiftCode = 27,


        [Display(Description = "ThisGiftCodeHasBeenUsedByAnotherUser")]
        ThisGiftCodeHasBeenUsedByAnotherUser = 28,

        [Display(Description = "ThisGiftCodeHasBeenUsed")]
        ThisGiftCodeHasBeenUsed = 29,
        [Display(Description = "ThisGiftIsForAnotherEmail")]
        ThisGiftIsForAnotherEmail = 30,

        [Display(Description = "ThisGiftCodeAlreadyHasBeenClaimedByYou")]
        ThisGiftCodeAlreadyHasBeenClaimedByYou = 31,
        [Display(Description = "You have Active Appointment for this service in this clininc")]
        YouAlreadyHaveABookedAppoiintment = 32,

    }
}
