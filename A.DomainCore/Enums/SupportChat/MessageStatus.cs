﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum MessageStatus
    {
       
        [Display(Description = "Sent")]
        Sent = 1,
        [Display(Description = "Read")]
        Read = 2,
       
    }



    }
