﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum Status
    {
        [Display(Description = "غیر فعال")]
        Deactive = 0,
        [Display(Description = "فعال")]
        Active = 1,
        [Display(Description = "حذف شده")]
        Deleted = 2,
        [Display(Description = "رد شده")]
        Rejected = 3

    }



    }
