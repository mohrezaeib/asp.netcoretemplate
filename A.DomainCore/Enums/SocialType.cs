﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum SocialType
    {
        [Display(Description = "FollowUs")]
        FollowUs = 1,
        [Display(Description = "ContactUs")]
        ContactUs = 2,


    }
}
