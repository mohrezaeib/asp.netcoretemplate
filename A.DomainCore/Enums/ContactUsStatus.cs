﻿using System.ComponentModel.DataAnnotations;

namespace DomainCore.Enums
{
    public enum ContactUsStatus
    {
        [Display(Description = "Pending")]
        Pending = 1,
        [Display(Description = "Seen")]
        Seen = 2,


    }
}
