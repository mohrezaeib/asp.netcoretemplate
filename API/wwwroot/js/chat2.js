//se strict";
"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl(
    "http://atlantisapi.nicode.org/chat", {
    skipNegotiation: true,
    transport: signalR.HttpTransportType.WebSockets,

    accessTokenFactory: () => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMTExMTExMS0xMTExLTExMTEtMTExMS0xMTExMTExMTExMTIiLCJpc3MiOiJodHRwOi8vQXRsYW50aXNBcGkubmljb2RlLm9yZyIsImlhdCI6MTYxODEzNzUzNCwiZXhwIjoxNjIxNDcwODM0LCJ1bmlxdWVfbmFtZSI6IjExMTExMTExLTExMTEtMTExMS0xMTExLTExMTExMTExMTExMiJ9.E1Y-OGUAhjZf3yAxAQxZL5H9PvDvJXPwWZLpN0oRFPk"
}).build();


//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;




connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
    connection.invoke("RegisterConnection", { isAdmin: true }).catch(function (err) {
        return console.error(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
});


connection.on("StartSupportChatResponse", function (Arguments) {
    console.log("From server to ClientStartSupportChat " + Arguments + "  . ")
});


connection.on("NewMessageForAdmin", function (arg) {
    console.log("From server to ClientStartSupportChat " + arg + "  . ")
});
document.getElementById("sendButton").addEventListener("click", function (event) {
    var Arguments = document.getElementById("Arguments").value;
    console.log("You  called on server  " + "with " + Arguments.toString() + "")

    
    event.preventDefault();
});





