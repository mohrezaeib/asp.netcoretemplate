//se strict";
"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl(
    "http://localhost:5001/chat", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets,

        accessTokenFactory: () => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMTExMTExMS0xMTExLTExMTEtMTExMS0xMTExMTExMTExMTIiLCJpc3MiOiJodHRwOi8vQXRsYW50aXNBcGkubmljb2RlLm9yZyIsImlhdCI6MTYxODEzNzUzNCwiZXhwIjoxNjIxNDcwODM0LCJ1bmlxdWVfbmFtZSI6IjExMTExMTExLTExMTEtMTExMS0xMTExLTExMTExMTExMTExMiJ9.E1Y-OGUAhjZf3yAxAQxZL5H9PvDvJXPwWZLpN0oRFPk"
}).build();


//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;




connection.start().then(function() {
    document.getElementById("sendButton").disabled = false;
}).catch(function(err) {
    return console.error(err.toString());
});


connection.on("StartSupportChatResponse", function (Arguments) {
    console.log("From server to ClientStartSupportChat " + Arguments  + "  . " )
});


connection.on("RegisterConnection", function (arg) {
    console.log("From server to ClientStartSupportChat " + arg + "  . ")
});
document.getElementById("sendButton").addEventListener("click", function(event) {
    var Arguments = document.getElementById("Arguments").value;
    console.log("You  called on server  " + "with " + Arguments.toString() + "")

    connection.invoke("SendMessage", { "DocumentId": null, "IsAdmin": false, "ChatId": "0fdc9289-6f6c-454b-a7ed-08d8fe8209c4", "text": "Hello", "TempId": "4fadbc90-9c68-11eb-acd2-234c8480a60b", "SenderId": "f82a1d47-d40f-4535-08cd-08d8fe82cb42" }).catch(function(err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});





