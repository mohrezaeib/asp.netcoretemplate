﻿using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Utility.Tools;
using Utility.Tools.Auth;

using Utility.Tools.SMS.Rahyab;
using Utility.Tools.Swager;

using DomainCore.General;

using Atlantis.DomainServices;
using Utility.Tools.SMS;

using Utility.Tools.EmailServies;

using DomainCore.Entities;
using Microsoft.Extensions.Hosting;
using WebApi.Middleware;
using Microsoft.AspNetCore.Http;
using EndPointWeb.Hubs;
using DomainData.Ef.Extentions;
using DomainData.Ef.Context;
using DomainData.Interfaces;
using DomainData.Ef.Repositories;
using DomainData.Ef;

namespace NewMountain
{
    public class Startup
    {



        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwager();
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddSessionStateTempDataProvider().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            Configuration.GetSection<AdminSettings>();
            services.AddEntityFrameworkSqlServer().AddDbContext<MainContext>(opt =>
                opt.UseSqlServer(
                Configuration.GetConnectionString("Default"), b => b.MigrationsAssembly("API"))

            );


            services.AddCors(options =>
            {
                options.AddPolicy("MyCorsPolicy",
                        builder => builder
                         .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .WithOrigins(
                            "http://*.nicode.org",
                            "http://nicode.org",
                            "http://*.atlantisclinics.ca",
                            "http://atlantisclinics.ca",
                            "http://localhost:4200",
                            "http://127.0.0.1:5500"
                            )
                            .AllowAnyMethod()
                             .WithExposedHeaders("content-disposition")
                            .AllowAnyHeader()
                     .AllowCredentials()
                     .SetPreflightMaxAge(TimeSpan.FromMinutes(2))

                    );
            });

            services.AddSignalR(hubOptions =>
            {
                hubOptions.EnableDetailedErrors = true;
                hubOptions.ClientTimeoutInterval = TimeSpan.FromMinutes(5);
                hubOptions.KeepAliveInterval = TimeSpan.FromMinutes(5);
            });

            services.AddJwt(Configuration);
            services.AddRepositories();
            services.AddApplicationServices();
            services.AddAutoMapperExtention();

            services.AddScoped<IEncrypter, Encrypter>();
            services.AddDbContext<IContext, MainContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddMemoryCache();
            services.AddLazyCache();

            services.AddScoped<INotification, RahyabService>();
            services.AddScoped<IEmailService, EmailService>();
            services.FireBaseAdminSDK(Configuration);
            services.AddScoped<IFireBase, FireBase>();
            services.AddSingleton<ISupportChatHub, SupportChatHub>();
            services.AddTwilio();

            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );


            services.AddApplicationInsightsTelemetry();
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ICurrentUser, CurrentUser>();


        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //  app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            // app.UseCookiePolicy();

            app.UseRouting();
            // app.UseRequestLocalization();
            app.UseCors("MyCorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SupportChatHub>("/chat");
            });
            app.UseMiddleware<JwtMiddleware>();

            //app.UseSession();
            //app.UseResponseCompression();
            //app.UseResponseCaching();
            app.UseMvcWithDefaultRoute(); ;
            app.ConfigureSwager();
            app.UseDefaultFiles();


        }


    }
}
