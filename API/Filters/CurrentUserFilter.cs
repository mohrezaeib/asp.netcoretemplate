﻿using DomainCore.AppUsers;
using DomainCore.Entities;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Filters
{
    public class CurrentUserFilter : IAsyncActionFilter
    {

        public async Task OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next
            )
        {
            var services = context.HttpContext.RequestServices;

            var session = services.GetService(typeof(ICurrentUser)) as CurrentUser;

            //var identity = context.HttpContext.User.Identity as ClaimsIdentity;
            //if (identity != null)
            //{
            //    var basedToken = context.HttpContext.Request.Headers["Authorization"].ToString();

            //    if (basedToken != null && basedToken != "")
            //    {
            //        var token = basedToken.Split(' ')[1];
            //        var handler = new JwtSecurityTokenHandler();
            //        var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
            //        var userIdString = jwtToken.Claims.FirstOrDefault(p => p.Type == "sub")?.Value;
            //        Guid userId = new Guid();


            //        if (Guid.TryParse(userIdString, out userId))
            //        {
            //            session.UserId = userId;
            //        }

            //    }
            //}
            context.HttpContext.Items.TryGetValue("UserId" , out var userId ) ;
            if (userId != null) session.UserId = Guid.Parse(userId.ToString());
            var resultContext = await next();


        }
    }
}
