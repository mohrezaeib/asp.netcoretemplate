﻿
using DomainCore.Enums;
using DomainData.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EndPointWeb.Filters
{
    public class AuthorizeAdmin : ActionFilterAttribute
    {
        //public string Type { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //Type = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName;
            IUnitOfWork unit = (IUnitOfWork)context.HttpContext.RequestServices.GetService(typeof(IUnitOfWork));
            if (context.ActionArguments.TryGetValue("dto", out object value))
            {
                var identity = context.HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var basedToken = context.HttpContext.Request.Headers["Authorization"].ToString();
                    if (basedToken != null && basedToken != "")
                    {
                        var token = basedToken.Split(' ')[1];
                        var handler = new JwtSecurityTokenHandler();
                        var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                        var userId = jwtToken.Claims.FirstOrDefault(p => p.Type == "sub").Value;

                        var user = unit.Users.Get(Guid.Parse(userId));



                       

                        //check if the user is Admin
                        if(user.UserRole == null || !user.UserRole.Any(p => p.RoleId == Roles.Admin))
                        {
                            context.Result = new StatusCodeResult(401);

                        }


                    }
                    else
                        context.Result = new StatusCodeResult(401);
                }
                else
                    context.Result = new StatusCodeResult(401);
            }


        }
    }
}