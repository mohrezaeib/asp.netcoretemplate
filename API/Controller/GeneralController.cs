﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.Slider;

namespace API.Controllers
{

    public class GeneralController : SimpleController
    {
        private readonly IGeneralService generalService;
        private readonly ICityService cityService;
        private readonly ISliderCRUD sliderCRUD;

        public GeneralController(
            IGeneralService generalService,
            ICityService cityService, 
            ISliderCRUD sliderCRUD  

            )
        {
            this.generalService = generalService;
            this.cityService = cityService;
            this.sliderCRUD = sliderCRUD;
        }


        [HttpGet]
        public async Task<ApiResult<SplashResultDto>> GetSpash([FromQuery] GetSplashDto dto)
        {
            return await generalService.GetSplash(dto);
        }
        

        [HttpGet]
        public async Task<ApiListResult<SliderDto>> GetSliders()
        {
            return await sliderCRUD.GetAllSlider();
        }
        [HttpGet]
        //[ResponseCache()]
        public async Task<ApiListResult<ProvinceDto>> GetProvinces()
        {
            return await cityService.GetProvines();
        }

        [HttpGet]
        public async Task<ApiListResult<CityDto>> GetByProvinceId([FromQuery] BaseByIntDto dto)
        {
            return await cityService.GetByProvinceId(dto);
        }



    }

}