﻿using Microsoft.AspNetCore.Mvc;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.UserActions;

namespace API.Controllers
{

    public class CustomerCommentController : SimpleController
    {
        private readonly IUserService userService;
        private readonly ICommentService commentService;

        public CustomerCommentController(
            IUserService userService,
            ICommentService commentService
          

            )
        {
            this.userService = userService;
            this.commentService = commentService;
        }


        [HttpPost]
        public async Task<ApiResult<CommenDtoForMobile>> AddCommentByUser([FromBody] AddCommentDto dto)
        {
            return await commentService.AddCommentByUser(dto);
        }


    }

}