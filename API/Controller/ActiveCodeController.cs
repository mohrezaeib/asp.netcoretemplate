﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using API.Controllers.BaseControllers;

namespace API.Controllers
{
    
    public class ActiveCodeController : SimpleController
    {
        private readonly ITwilioRestClient _client;
        private readonly IActiveCodeService activeCodeService;

        public ActiveCodeController(ITwilioRestClient client,
            IActiveCodeService activeCodeService


            )
        {
            _client = client;
            this.activeCodeService = activeCodeService;
        }

  
        [HttpPost]
        public async  Task<BaseApiResult> SendActivecode([FromBody] SendActiveCodeDto dto)
        {
            return await activeCodeService.SendActiveCode(dto);
        } 
        [HttpPost]
        public async Task<ApiResult<UserDto>> CheckActivecode([FromBody] CheckActiveCodeDto dto)
        {
            return await activeCodeService.CheckActiveCode(dto);
        }

        //[HttpPost]
        //public TwiMLResult ReceiveSms([FromForm] SmsRequest incomingMessage)
        //{
        //    var messagingResponse = new MessagingResponse();
        //    messagingResponse.Message("Thank you. Your message was received.");

        //    return TwiML(messagingResponse);
        //}
    }
  
}