﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using Microsoft.AspNetCore.Authentication;

namespace API.Controllers
{

    public class UserController : SimpleController
    {
        private readonly IUserService userService;

        public UserController(
            IUserService userService
            )
        {
            this.userService = userService;
        }


        [HttpPost]
        public async Task<ApiResult<UserDto>> EditProfile([FromBody] EditProfileDto dto)
        {
            return await userService.EditProfile(dto);
        } 

        [HttpPost]
        public async Task<ApiResult<UserDto>> EditUserSetting([FromBody] EditUserSettingDto dto)
        {
            return await userService.EditUserSetting(dto);
        } 

        [HttpPost]
        public async Task<ApiResult<UserDto>> SetPassword([FromBody] SetPasswordByUserDto dto)
        {
            return await userService.SetPassword(dto);
        }

        [HttpGet]
        public async Task<ApiResult<UserDto>> GetProfile()
        {

            var token = await HttpContext.GetTokenAsync("access_token");
            var userId = userService.GetUserIdFromToken(token);
            return await userService.GetUser(new BaseByGuidDto { Id= userId});

        }
    

    }

}