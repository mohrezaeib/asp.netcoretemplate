﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.Slider;
using Utility.Tools.EmailServies;

namespace API.Controllers
{

    public class TestController : SimpleController
    {
        private readonly IGeneralService generalService;
        private readonly ICityService cityService;
        private readonly IEmailService emailService;

        public TestController(
            IGeneralService generalService,
            ICityService cityService,
            IEmailService emailService

            )
        {
            this.generalService = generalService;
            this.cityService = cityService;
            this.emailService = emailService;
        }


        [HttpPost]
        public async Task<string> SendMail([FromBody] mailDTo dto)
        {
            return await emailService.SendAsync(dto.text, dto.mail);
        }
        public class mailDTo
        {
            public string mail { get; set; }
            public string text { get; set; }
        }
        [HttpGet]
        public async Task<object> Get([FromQuery] TimeSpan t)
        {

            var time = t;
            return time.ToString();
        }
        //[HttpGet]
        //[ResponseCache()]
        //public async Task<ApiListResult<ProvinceDto>> GetProvinces()
        //{
        //    return await cityService.GetProvines();
        //}

        //[HttpGet]
        //public async Task<ApiListResult<CityDto>> GetByProvinceId([FromQuery] BaseByIntDto dto)
        //{
        //    return await cityService.GetByProvinceId(dto);
        //}



    }

}