﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;

namespace API.Controllers
{

    public class AdminUserController : SimpleController
    {
        private readonly IUserService userService;

        public AdminUserController(
            IUserService userService
            )
        {
            this.userService = userService;
        }


        [HttpPost]
        public async Task<ApiResult<UserDto>> AddUser([FromBody] AddUserByAdminDto dto)
        {
            return await userService.AddUserByAdmin(dto);
        } 
        [HttpPut]
        public async Task<ApiResult<UserDto>> EditUser([FromBody] EditProfileByAdminDto dto)
        {
            return await userService.EditUserByAdmin(dto);
        }

        [HttpGet]
        public async Task<ApiResult<UserDto>> GetUserById([FromQuery] BaseByGuidDto dto)
        {
            return await userService.GetUser(dto);

        }  
        [HttpGet]
        public async Task<ApiPageResult<UserDto>> GetUsersByFilter([FromQuery] GetUsersByFilterDto dto)
        {
            return await userService.GetUsersByFilter(dto);

        }



    }

}