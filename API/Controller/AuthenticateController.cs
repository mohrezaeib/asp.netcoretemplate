﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using EndPointWeb.Filters;

namespace API.Controllers
{

    public class AuthenticateController : SimpleController
    {
        private readonly ILoginService loginService;
        private readonly IUserService userService;

        public AuthenticateController(
            ILoginService loginService, IUserService userService


            )
        {
            this.loginService = loginService;
            this.userService = userService;
        }


        [HttpPost]
        public async Task<ApiResult<UserDto>> Login([FromBody] LoginDto dto)
        {
            return await loginService.Login(dto);
        }

        [HttpPost]
        public async Task<ApiResult> ForgotPassWord([FromBody] ForgotPasswordDto dto)
        {
            return await loginService.ForgotPassWord(dto);
        }
        //TODO
        [AuthorizeUser]
        [HttpPost]
        public async Task<ApiResult> ChangePassword([FromBody] ChangePasswordDto dto)
        {
            return await loginService.ChangePassWord(dto);
        }
        [HttpPost]
        public async Task<BaseApiResult> RegisterByEmail([FromBody] RegisterByEmailDto dto)
        {
            return await loginService.RegisterByEmail(dto);
        }

        [HttpPost]
        public async Task<ApiResult<UserDto>> ConfirmEmail([FromBody] ConfirmEmailDto dto)
        {
            var result  = await loginService.ConfirmEmail(dto);
            return result;

        }
        [AuthorizeUser]
        [HttpPost]
        public async Task<ApiResult<UserDto>> SetPassword([FromBody] SetPasswordByUserDto dto)
        {
            var result  = await userService.SetPassword(dto);
            return result;

        }



    }

}