﻿using System;
using Microsoft.AspNetCore.Mvc;

using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;
using Microsoft.AspNetCore.Authentication;
using DomainCore.DTO.UserActions;

namespace API.Controllers
{

    public class UserActionController : SimpleController
    {
        private readonly IUserService userService;
        private readonly ICommentService commentService;
        private readonly IUserFavoriteService userFavoriteService;
 

        public UserActionController(
            IUserService userService,
            ICommentService commentService, 
            IUserFavoriteService userFavoriteService
   
            
          

            )
        {
            this.userService = userService;
            this.commentService = commentService;
            this.userFavoriteService = userFavoriteService;
 
        }


        [HttpPost]
        public async Task<ApiResult<CommenDtoForMobile>> AddCommentByUser([FromBody] AddCommentDto dto)
        {
            return await commentService.AddCommentByUser(dto);
        }
        [HttpPost]
        public async Task<ApiResult<UserFavoriteDto>> AddUserFavoriteByUser([FromBody] AddUserFavoriteDto dto)
        {
            return await userFavoriteService.AddUserFavoriteByUser(dto);
        }
        [HttpDelete]
        public async Task<BaseApiResult> DeleteUserFavorite([FromBody] AddUserFavoriteDto dto)
        {
            return await userFavoriteService.DeleteUserFavorite(dto);
        }

        [HttpGet]
        public async Task<ApiResult<UserFavoriteListDto>> GetUserFavoritesByUserId([FromQuery] BaseByGuidDto dto)
        {
            return await userFavoriteService.GetUserFavoritesByUserId(dto);
        }
        
    }

}