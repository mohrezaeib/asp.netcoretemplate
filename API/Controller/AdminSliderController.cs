﻿using System;
using Microsoft.AspNetCore.Mvc;

using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.Base;
using DomainCore.DTO.Slider;

namespace API.Controllers
{
    
    public class AdminSliderController : SimpleController
    {
        private readonly ISliderCRUD SliderSlider;

        public AdminSliderController(
            ISliderCRUD SliderSlider


            )
        {
            this.SliderSlider = SliderSlider;
        }

  
       
        [HttpPost]
        public async Task<ApiResult<SliderDto>> AddSlider([FromBody] AddSliderDto dto)
        {
            return await SliderSlider.AddSlider(dto);
        }
        [HttpPut]
        public async Task<ApiResult<SliderDto>> EditSlider([FromBody] EditSliderDto dto)
        {
            return await SliderSlider.EditSlider(dto);
        }
        [HttpDelete]
        public async Task<BaseApiResult> DeleteSlider([FromQuery] BaseByIntDto dto)
        {
            return await SliderSlider.DeleteSlider(dto);
        }
        [HttpGet]
        public async Task<ApiResult<SliderDto>> GetSliderById([FromQuery] BaseByIntDto dto)
        {
            return await SliderSlider.GetSliderById(dto);
        }
        [HttpGet]
        public async Task<ApiListResult<SliderDto>> GetAllSlider()
        {
            return await SliderSlider.GetAllSlider();
        }

     
    }

}