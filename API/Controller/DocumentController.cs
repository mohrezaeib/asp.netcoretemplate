﻿using System;
using Microsoft.AspNetCore.Mvc;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.General;
using DomainCore.Dto.Base;
using DomainCore.Dto.User;
using DomainCore.DTO.Base;
using API.Controllers.BaseControllers;


namespace API.Controllers
{

    public class DocumentController : SimpleController
    {
        public IDocumentService documentService { get; }

        public DocumentController(
            IDocumentService documentService


            )
        {
            this.documentService = documentService;
        }


        [HttpPost]
        public async Task<ApiResult<DocumentDto>> Send([FromForm] SendDocumentDto dto)
        {
            dto.File = Request.Form.Files[0];
            return await documentService.Send(dto);
        }

        [HttpGet]
        public async Task<ApiResult<DocumentDto>> Get([FromQuery] BaseByGuidDto dto)
        {
            return await documentService.Get(dto);
        }



    }

}