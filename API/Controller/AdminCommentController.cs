﻿using Microsoft.AspNetCore.Mvc;

using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.Base;
using DomainCore.DTO.UserActions;

namespace API.Controllers
{
    
    public class AdminCommentController : SimpleController
    {
        private readonly ICommentService CommentService;

        public AdminCommentController(

            ICommentService CommentService
            )
        {
            this.CommentService = CommentService;
        }

  
      

        [HttpPost]
        public async Task<ApiResult<CommentBigDto>> AddAnswerByAdmin([FromBody] AddCommentAnswerDto dto)
        {
            return await CommentService.AddAnswerByAdmin(dto);
        }
        [HttpPut]
        public async Task<ApiResult<CommentBigDto>> EditComment([FromBody] EditCommentDto dto)
        {
            return await CommentService.EditComment(dto);
        }
        [HttpDelete]
        public async Task<BaseApiResult> DeleteComment([FromQuery] BaseByGuidDto dto)
        {
            return await CommentService.DeleteComment(dto);
        }
        [HttpGet]
        public async Task<ApiResult<CommentBigDto>> GetCommentById([FromQuery] BaseByGuidDto dto)
        {
            return await CommentService.GetCommentById(dto);
        }
        [HttpGet]
        public async Task<ApiPageResult<CommentBigDto>> GetCommentsByFilter([FromQuery] GetCommentByFiltrerDto dto)
        {
            return await CommentService.GetCommentsByFilter(dto);
        }

    }

}