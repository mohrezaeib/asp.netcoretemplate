﻿using Microsoft.AspNetCore.Mvc;

using DomainServices.Contracts;
using System.Threading.Tasks;
using DomainCore.Dto.Base;
using API.Controllers.BaseControllers;
using DomainCore.DTO.AppMore;

namespace API.Controllers
{
    
    public class AppInfoController : SimpleController
    {
        private readonly IAboutUsCRUD AboutUsCRUD;

        private readonly IFaQCategoryCRUD FaQCategoryCRUD;
        private readonly IContactUsCRUD ContactUsCRUD;

        public AppInfoController(
            ISocialCRUD  socialCRUD , 
            IAboutUsCRUD aboutUsCRUD,
            IFaQCRUD FaQCRUD, 
            IFaQCategoryCRUD faQCategoryCRUD,
            IContactUsCRUD contactUsCRUD


            )
        {
            this.AboutUsCRUD = aboutUsCRUD;
            this.FaQCategoryCRUD = faQCategoryCRUD;
            this.ContactUsCRUD = contactUsCRUD;
        }



      

        [HttpGet]
        public async Task<ApiListResult<FaQCategoryDto>> GetAllFaQCategoryWithDetail()
        {
            return await FaQCategoryCRUD.GetAllFaQCategoryWithDetail();
        }

       
        [HttpPost]
        public async Task<ApiResult<ContactUsDto>> AddContactUs([FromBody] AddContactUsDto dto)
        {
            return await ContactUsCRUD.AddContactUs(dto);
        }



        [HttpGet]
        public async Task<ApiResult<AboutUsDto>> GetAboutUs()
        {
            return await AboutUsCRUD.GetAboutUs();
        }

      
    }

}