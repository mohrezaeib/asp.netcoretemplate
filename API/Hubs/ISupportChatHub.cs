﻿using DomainCore.DTO.Base;
using DomainCore.DTO.SupportChat;
using DomainCore.Entities.SupportChat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EndPointWeb.Hubs
{
    public interface ISupportChatHub
    {
        Task OnConnectedAsync();
        Task RegisterConnection(RegisterConnectionDto dto);
        Task OnDisconnectedAsync(Exception exception);
        Task StartSupportChat();
        Task GetChatByFilter(GetChatByFilterDto dto);
        Task SendMessage(SendMessageDto dto);
        Task SendMessageByAdmin(SendMessageDto dto);
        Task MarkChatAsReadByAdmin(BaseByGuidDto dto);
        Task BroadCastNewChat(Chat chat);
        Task BroadCastMarkChatAsReadByAdmin(Chat chat);
        Task BroadCastNewMessageForAdmin(Message msg);
        Task BroadCastNewMessageForCustomer(Message msg);






    }
}
