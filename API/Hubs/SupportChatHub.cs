﻿

using Microsoft.AspNetCore.SignalR;


using System;
using System.Threading.Tasks;

using System.Collections.Generic;
using Newtonsoft.Json;
using DomainCore.Entities;
using DomainServices.Contracts;
using Utility.Tools.General;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using DomainCore.DTO.SupportChat;
using DomainCore.DTO.Base;
using DomainCore.Entities.SupportChat;
using AutoMapper;
using System.Linq;
using DomainData.Interfaces;

namespace EndPointWeb.Hubs
{



    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SupportChatHub : Hub, ISupportChatHub
    {



        private readonly static ConnectionMapping<Guid> customerConnections =
           new ConnectionMapping<Guid>();
        private readonly static ConnectionMapping<Guid> adminConnections =
     new ConnectionMapping<Guid>();
        private readonly ICurrentUser currentUser;
        private readonly IUnitOfWork unit;
        private readonly ICustomerSupportChat customerSupportChat;
        private readonly IAdminSupportChat adminSupportChat;
        private readonly IMapper mapper;

        public SupportChatHub(
          ICurrentUser currentUser,
          IUnitOfWork unit,
          ICustomerSupportChat customerSupportChat,
          IAdminSupportChat adminSupportChat,
          IMapper mapper



            )
        {
            this.currentUser = currentUser;
            this.unit = unit;
            this.customerSupportChat = customerSupportChat;
            this.adminSupportChat = adminSupportChat;
            this.mapper = mapper;
        }







        public override async Task OnConnectedAsync()
        {
            //if (IsAdmin)
            //    adminConnections.Add(currentUser.UserId.Value, Context.ConnectionId);
            //else
            //    customerConnections.Add(currentUser.UserId.Value, Context.ConnectionId);
             await base.OnConnectedAsync();

        }
        public async Task RegisterConnection(RegisterConnectionDto dto  )
        {
            if (dto.IsAdmin)
                adminConnections.Add(currentUser.UserId.Value, Context.ConnectionId);
            else
                customerConnections.Add(currentUser.UserId.Value, Context.ConnectionId);

        }


        public override async Task OnDisconnectedAsync(Exception exception)
        {

            customerConnections.Remove(currentUser.UserId.Value, Context.ConnectionId);
            adminConnections.Remove(currentUser.UserId.Value, Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }




        public async Task StartSupportChat()
        {

            var result = await customerSupportChat.StartSupportChat();
            if (result.Status) result.Data.IsAnySupportOnline = adminConnections.GetAllConnections().Any();
            await Clients.Caller.SendAsync("StartSupportChatResponse", Agent.ToCamelCaseJson(result) );


        }
        public async Task GetChatByFilter(GetChatByFilterDto dto)
        {

            var result = await adminSupportChat.GetChatByFilter(dto);
            await Clients.Caller.SendAsync("GetChatByFilterResponse", Agent.ToCamelCaseJson(result));


        }


        public async Task SendMessage(SendMessageDto dto)
        {

            var result = await customerSupportChat.SendMessage(dto);
            
            
            
            await BroadCastNewMessageForAdmin(result.Data);
            await BroadCastNewMessageForCustomer(result.Data);
          //  await Clients.Caller.SendAsync("", Agent.ToCamelCaseJson(result));


        }
        public async Task SendMessageByAdmin(SendMessageDto dto)
        {

            var result = await adminSupportChat.SendMessage(dto);
            await BroadCastNewMessageForAdmin(result.Data);
            await BroadCastNewMessageForCustomer(result.Data);
            //  await Clients.Caller.SendAsync("ClientMessageSentByAdmin", Agent.ToCamelCaseJson(result));


        }
        public async Task MarkChatAsReadByAdmin(BaseByGuidDto dto)
        {

            var result = await adminSupportChat.MarkChatAsRead(dto);
            await BroadCastMarkChatAsReadByAdmin(result.Data);
          //  await Clients.Caller.SendAsync("MarkChatAsReadByAdmin", Agent.ToCamelCaseJson(result));


        }
        public async Task BroadCastNewChat(Chat chat)
        {

            var admins = adminConnections.GetAllConnections();
            var dto = mapper.Map<ChatDto>(chat);
            await Clients.Clients(admins).SendAsync("NewChatIsAdded", Agent.ToCamelCaseJson(dto));
        }
        public async Task BroadCastMarkChatAsReadByAdmin(Chat chat)
        {

            var admins = adminConnections.GetAllConnections();
            var dto = mapper.Map<ChatDto>(chat);
            await Clients.Clients(admins).SendAsync("ChatMarkedAsRead", Agent.ToCamelCaseJson(dto));
        }

        public async Task BroadCastNewMessageForAdmin(Message msg)
        {
            var dto = mapper.Map<MessageDto>(msg);
            var admins = adminConnections.GetAllConnections();
            await Clients.Clients(admins).SendAsync("NewMessageForAdmin", Agent.ToCamelCaseJson(dto));

        }
        public async Task BroadCastNewMessageForCustomer(Message msg)
        {
            var dto = mapper.Map<MessageDto>(msg);
            var conections = customerConnections.GetConnections(msg.Chat.CustomerId).ToList() ;
            await Clients.Clients(conections).SendAsync("NewMessageForCustomer", Agent.ToCamelCaseJson(dto));

        }
       

    }




}
